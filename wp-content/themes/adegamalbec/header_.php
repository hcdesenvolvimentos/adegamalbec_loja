<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adegamalbec
 */
global $woocommerce;
global $configuracao;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );

$cart_subtotal = $woocommerce->cart->get_cart_subtotal();

global $configuracao;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href=" <?php echo get_template_directory_uri(); ?>/favicon.ico">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67616006-17', 'auto');
  ga('send', 'pageview');

</script>
<meta property="og:title" content="Adega Malbec" />
<meta property="og:description" content="Adega Malbec - Vinhos e Presentes" />
<meta property="og:url" content="http://www.adegamalbec.com.br/" />
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/compartilhar.png"/>
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="Adega Malbec" />
</head>

<body <?php body_class(); ?>>

<div class="loader"></div>
<div class="modal" id="modalCartSuccess" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content modalCartSuccess">
			<i class="fa fa-shopping-cart"></i>
			<i class="fa fa-plus"></i>
			<strong>Sucesso</strong>
			<p>Produto adicionado no carrinho com sucesso!</p>
			<a href="<?php echo $urlCarrinho; ?>">Ir para o carrinho</a>
			<button id="modalCartSuccessFechar" class="btn btn-default" data-dismiss="modalCartSuccess">Fechar</button>
		</div>
	</div>
</div>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>
		(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<?php
	global $woocommerce;
	// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
	$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
	$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';

?>

<!-- IMAGENS DAS PAIS CATEGORIAS DO MENU / RECUPERANDO POR REDUX-->
<?php
	$Imgcategoryvinho = $configuracao['opt-vinho']['url'];
	$Imgcategoryespumante = $configuracao['opt-espumante']['url'];
	$Imgcategorycervejas = $configuracao['opt-cervejas']['url'];
	$Imgcategorycestas_presentes = $configuracao['opt-cestas-Presenter']['url'];
	$Imgcategoryacessorios_utilidades = $configuracao['opt-acessorios-Utilidades']['url'];

	// LINKS
	$ImgcategoryvinholLink = $configuracao['opt-vinho-link'];
	$ImgcategoryespumantelLink = $configuracao['opt-espumante-link'];
	$ImgcategorycervejaslLink = $configuracao['opt-cervejas-link'];
	$Imgcategorycestas_presenteslLink = $configuracao['opt-Presenter-link'];
	$Imgcategoryacessorios_utilidadeslLink = $configuracao['opt-acessorios-link'];
	
 ?>	
<!-- HEADER -->
	<header class="topo">

		<div class="area-topo">
			<div class="container">
				<div class="row ">
					<div class="col-md-2">
						<!--LOGO  -->
						<a href="<?php echo home_url('/'); ?>" class="link-logo" title="Adega Malbec">
							<h1>Adega Malbec</h1>
						</a>
					</div>
					<div class="col-md-10">
						<ul class="ulTopo">
							<li class="liAreaPesquisa">
								<!-- ÁREA DE PESQUISA -->
								<div class="area-pesquisa">
									<input class="pesquisar" type="submit" id="yith-searchsubmit" >
									 <?php  echo do_shortcode('[yith_woocommerce_ajax_search]'); ?>
								</div>
							</li>
							<li>
								<!-- ÁREA INFO CONTA -->
								<div class="area-info-conta">
									<?php if (is_user_logged_in() == true) { ?>
									<span>Bem vindo(a), <?php echo $current_user->user_login; ?>.</span>
									<?php } else{ ?>
									<a href="" data-toggle="modal" data-target=".bs-example-modal-sm" >Acesse sua conta</a>
									<?php }  ?>
									<a href="<?php echo home_url('pagina-de-ajuda'); ?>">Ajuda</a>
									<?php if (is_user_logged_in() == true) { ?>
									<a href="<?php echo $urlLogout ?>" id="sair">Sair</a>
									<?php } ?>
								</div>
							</li>
							<li class="liAreaCarrinho">
								<!-- ÁREA  CARRINHO -->
								<div class="area-carrinho">
									<div class="menu-minhaconta">
									<img src="<?php bloginfo('template_directory'); ?>/img/icon.png" alt="">
										<a href="<?php echo $urlCarrinho ?>">Minha Sacola <?php echo $cart_subtotal ?><i class="fa fa-angle-down"></i></a>
									</div>
									<span class="qtd-header" id="qtd-header" ><?php echo $qtdItensCarrinhoRotulo;?></span>
								</div>
								<div class="informacoes-carrinho">
									<?php dynamic_sidebar( 'sidebar-2' ); ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- MENU  -->
		<div class="navbar" role="navigation">
				<div class="container">
				<!-- MENU MOBILE TRIGGER -->
				<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<!--  MENU MOBILE-->
				<div class="row navbar-header">

					<nav class="collapse navbar-collapse" id="collapse">

						<ul class="nav navbar-nav">
							<?php

								// LISTAR CATEGORIAS PAI
						        $argsCategorias = array(
										                'taxonomy'     => 'product_cat',
										                'child_of'     => 0,
										                'parent'       => 0,
										                'orderby'      => 'name',
										                'pad_counts'   => 0,
										                'hierarchical' => 1,
										                'title_li'     => '',
										                'hide_empty'   => 0
										        	);

						        $listaCategorias = get_categories($argsCategorias);

					            if($listaCategorias) {

					                foreach($listaCategorias as $categoria):
			                			if($categoria->category_nicename == "vinhos" || $categoria->category_nicename == "espumantes" || $categoria->category_nicename == "cervejas" || $categoria->category_nicename == "cestas-presentes" || $categoria->category_nicename == "acessorios-utilidades"):
			               	?>

							<li class="dropdown">

								<a href="<?php echo get_term_link($categoria->slug, 'product_cat') ?>" class="dropdown-toggle" ><?php echo $categoria->name  ?></a>

								<!-- LISTAR  SUB CATEGORIAS -->
								<?php

									$categoriaId = $categoria->term_id;
							        $argsSubCategorias = array(
											                'taxonomy'     => 'product_cat',
											                'child_of'     => 0,
											                'parent'       => $categoriaId ,
											                'orderby'      => 'name',
											                'pad_counts'   => 0,
											                'hierarchical' => 1,
											                'title_li'     => '',
											                'hide_empty'   => 0
											        	);

							        $listaSubCategorias = get_categories($argsSubCategorias);

							        // VERIFICANDO DE Á CATEGORIAS FILHAS
							        if($listaSubCategorias) {
						        ?>
								<ul class="dropdown-menu">

									<!-- MONTANDO SUB CATEOGIRAS -->
									<?php

								        if($listaSubCategorias) {

								            foreach($listaSubCategorias as $Subcategoria):
								        		$Subcategoria->name;

								   	?>

									<li>
										<small><?php echo $categoria->name; ?> por <?php echo $Subcategoria->name; ?></small>
										<!-- LISTAR  CATEGORIAS FILHAS -->
										<?php
											$categoriaFilhasId = $Subcategoria->term_id;
									        $argsCategoriasFilhas = array(
													                'taxonomy'     => 'product_cat',
													                'child_of'     => 0,
													                'parent'       => $categoriaFilhasId ,
													                'orderby'      => 'name',
													                'pad_counts'   => 0,
													                'hierarchical' => 1,
													                'title_li'     => '',
													                'hide_empty'   => 0
													        	);

									        $argslistarCategoriasFilhas = get_categories($argsCategoriasFilhas);

									        if($argslistarCategoriasFilhas) {

									             foreach($argslistarCategoriasFilhas as $categoriaFilhas):

									        		 $categoriaFilhas->name;

									        		// for ($i=0; $i < 7; $i++) {
									        		// 	$catFilhoNome = $argslistarCategoriasFilhas[$i]->name;
									        		// 	$catFilhoLink = $argslistarCategoriasFilhas[$i]->slug;
									        		// 	$catFilhoLink = get_term_link($catFilhoLink, 'product_cat');
									        		// 	//var_dump($catFilhoLink);exit;
									        		if ($Subcategoria->name == "País") {
								        			    	$SubcategoriaNome = "+ Ver todos os Países"  ;
			        			                		}elseif ($Subcategoria->name == "Tipo") {
			        			                			$SubcategoriaNome = "+ Ver todos os Tipos"  ;
			        			                		}elseif ($Subcategoria->name == "Uvas") {
			        			                			$SubcategoriaNome = "+ Ver todas as Uvas"  ;
			        			                		}

									   	?>
										<a href="<?php echo get_term_link($categoriaFilhas->slug, 'product_cat') //get_term_link($categoriaFilhas->slug, 'product_cat') ?>"><?php echo $categoriaFilhas->name;; //$categoriaFilhas->name;	?></a>
									<?php endforeach; } ?>

									<?php  $linkcategoria = get_term_link($Subcategoria->slug, 'product_cat') ?>
									<a href="<?php echo get_term_link($Subcategoria->slug, 'product_cat') ?>" class="link-ver-todos"> <?php echo $SubcategoriaNome ?></a>


									</li>
									<?php  endforeach; } ?>


									<!-- VERIFICANDO CATEGORIA PAI -->
									<?php
										$nome_categoria = $categoria->name;

										if ($categoria->name == "Vinhos") {
											$fotoCaterory = $Imgcategoryvinho;

										}elseif ($categoria->name == "Espumantes") {
											$fotoCaterory = $Imgcategoryespumante;
											$fotoCateroryLink = $ImgcategoryespumanteLink;
										}elseif ($categoria->name == "Cervejas") {
											$fotoCaterory = $Imgcategorycervejas;
											$fotoCateroryLink = $ImgcategorycervejaslLink;
										}elseif ($categoria->name == "Cestas & Presentes") {
											$fotoCaterory = $Imgcategorycestas_presentes;
											$fotoCateroryLink = $Imgcategorycestas_presentesLink;
										}elseif ($categoria->name == "Acessórios & Utilidades") {
											$fotoCaterory = $Imgcategoryacessorios_utilidades;
											$fotoCateroryLink = $Imgcategoryacessorios_utilidadesLink;
										}
									 ?>
									<li class="foto"><a href="<?php echo $fotoCateroryLink ?>"><img src="<?php echo $fotoCaterory ?>" class="img-responsive" alt=""></a></li>

								</ul>
								<?php } ?>
							</li>
							 <?php endif;
				                endforeach; }
							?>
							<li><a href="<?php echo home_url('/empresa-e-eventos/'); ?>">Para Empresas & Eventos</a></li>
							<li><a href="<?php echo home_url('promocoes/'); ?>"><span class="circulo-promocao"></span>Promoções</a></li>
							<li><a href="<?php echo home_url('recentes/'); ?>"><span class="lancamentos"></span>Lançamentos</a></li>

						</ul>

					</nav>

				</div>

			</div>

		</div>

		<!-- MODAL DE LOGIN -->
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			    <div class="modal-body">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

			      	<div class="login">
			      		<span>Login</span>
			      		<p>Entre para ter acesso a histórico de compras, cadastro e muito mais</p>
			      		<a href="http://adegamalbec.pixd.com.br/wp-login.php?ywsl_social=facebook&amp;redirect=http%3A%2F%2Fadegamalbec.pixd.com.br%2Fminha-conta%2F" class="face"><i class="fa fa-facebook" aria-hidden="true"></i>Entrar com facebook</a>
			      		<p>Ou entre com seu email ou usuário</p>
			      		<div class="form">
							<!-- FORMULÁRIO DE LOGIN  -->
		      				<form method="post" class="">

								<?php do_action( 'woocommerce_login_form_start' ); ?>
									<!-- LOGIN -EMAIL -->
									<input type="text" placeholder="E-mail ou usuário" class="" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />

									<!-- SENHA -->
									<input class="" placeholder="senha" type="password" name="password" id="password" />

								<?php do_action( 'woocommerce_login_form' ); ?>


								<?php wp_nonce_field( 'woocommerce-login' ); ?>
									<!-- LINK PARA REDEFINIR A SENHA -->
									<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
									<!-- INPUT PARA FALZER O LOGIN -->
									<input class="entrar" type="submit" class="" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />


								<?php do_action( 'woocommerce_login_form_end' ); ?>

							</form>

			      			<p>Ainda não possui uma cadastro</p>
			      			<!-- LINK PARA SE CADASTRAR -->
			      			<?php wp_nonce_field( 'woocommerce-register' ); ?>
			      			<?php echo '<a class="cadastrar" href="' .get_permalink(woocommerce_get_page_id('myaccount')). '?action=register">Cadastrar-se</a>'; ?>
			      			<?php do_action( 'woocommerce_register_form_end' ); ?>
			      		</div>
			      	</div>
			   	</div>
		    </div>
		  </div>
		</div>

	</header>
