<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package adegamalbec
 */

get_header(); ?>

	<!-- 404 -->
	<div class="pg pg-404">
		<div class="container">
			
			<span>Oops! não encontrou o que você queria ? </span>
			
			<div class="pesquisa">
				<form method="get" id="searchform" action="<?php echo home_url('/'); ?>" >							
							
					<input  placeholder="Pesquise por outra palavra aqui !" title="Buscar"  type="text" class="field" name="s" id="s"  >
					<button type="submit" class="btn enviar" name="submit" id="searchsubmit" value=""><i class="fa fa-search"></i></button>
				</form>
			</div>

			<div class="links">
				<a href="<?php echo home_url('contato/'); ?>"><i class="fa fa-phone"></i>Entre em contato </a> 
				<!-- <a href=""><i class="fa fa-shopping-cart"></i> Meu carrinho </a> -->
				<a href="<?php echo home_url('loja/'); ?>"><i class="fa fa-shopping-basket"></i> Voltar a loja </a>
			</div>



		</div>
	</div>
	<!--PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		<div class="container">

			

			<!-- CONTÚDO -->
			<div class="row">
				
				<!-- CONTEÚDO LOJA -->
				<div class="col-md-12">		
													
					<!-- PRODUTOS EM DESTAQUE -->
					<div class="produto-destaque">
						
						<span class="titulo-pg">veja algumas indicações</span>	

						<!-- BOTÕES DE NAVEGAÇÃO -->
						<div class="botoes-produtos">
							<button class="navegacaoDestque-produtoFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
							<button class="navegacaoDestque-produtoTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						</div>		
						
						<!-- PRODUTOS EM DESTAQUE -->
						<div class="carrossel-produtos-destaque" >
							<div id="carrossel-produtos-destaque" class="owl-Carousel">
								
								<!-- PRODUTO -->
								<?php
									$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 12, 'orderby' =>'date','order' => 'DESC' );
						            $loop = new WP_Query( $args );
						            // var_dump($loop);
						            while ( $loop->have_posts() ) : $loop->the_post(); global $product;
						            
								?>
								<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">										
												<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>
									
											<button class="comprar"><img src="<?php bloginfo('template_directory'); ?>/img/sacola.png" alt="">Adicionar ao carrinho</button>
											
										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h2><?php the_title(); ?></h2>
									</div>
									
							

									<!-- PREÇO -->
									<p class="preco"><?php echo $product->get_price_html(); ?></p>
									<?php 
									 	$price = $product->get_price() / 3;									
										 
									?>
									<p class="promocao"><span>ou</span>3X R$ <?php echo number_format($price,2,",","." ); ?></p>

									<button class="adicionar-aocarrinho"> comprar <!-- <b>6 un pague R$39,90/un</b> -->
										<span>
											<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
													sprintf( '<a rel="nofollow" class="opcoes" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
														esc_url( $product->add_to_cart_url() ),
														esc_attr( isset( $quantity ) ? $quantity : 1 ),
														esc_attr( $product->id ),
														esc_attr( $product->get_sku() ),
														esc_attr( isset( $class ) ? $class : 'button' ),
														esc_html( $product->add_to_cart_text() )
													),
												$product );
											?>
									 	</span>
									 </button>
									
								</div>						
								<?php endwhile;wp_reset_query(); ?>							

							</div>						
						</div>
					</div>

					
				</div>
			</div>				
		</div>
	</div>	

<?php
get_footer();
