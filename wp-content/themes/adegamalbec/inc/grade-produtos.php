
	<!-- PÁGINA LOJA  -->
	<div class="pg pg-loja" style="display:;">

			<div class="row">


				<!-- CONTEÚDO LOJA  -->
				<div class="col-md-12">
					<div class="conteudo-loja">

						<!-- PRODUTOS DA LOJA -->
						<div class="produtos-loja">
							<!-- PRODUTOS EM DESTAQUE -->
							<div class="carrossel-produtos-destaque" >

								<!-- PRODUTO -->
								<?php
									$args = array( 'post_type' => 'product', 'stock' => 1, 'posts_per_page' => 12, 'orderby' =>'date','order' => 'DESC' );

						            $loop = new WP_Query( $args );
						            while ( $loop->have_posts() ) : $loop->the_post(); global $product;

								?>
								<div class="item lista-grade">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">
											<!-- DESCONTO PORCENTAGEM -->
										<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

											<button class="comprar"><img src="<?php bloginfo('template_directory'); ?>/img/sacola.png" alt="">Adicionar ao carrinho</button>
										</div>
									</a>
									<div class="conteudo">
										<!-- DESCRIÇÃO -->
										<div class="descricao-produto">
											<h3><?php the_title(); ?></h3>
										</div>

										<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo'
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>

										<!-- PREÇO -->
									<p class="preco"><?php echo $product->get_price_html(); ?></p>
									<?php
									 	$price = $product->get_price() / 3;

									?>
									<p class="promocao"><span>ou</span>3X R$ <?php echo number_format($price,2,",","." ); ?></p>

										<div class="descricao-completa">
											<p><?php echo $product->post->post_content; ?></p>
										</div>
										<button class="adicionar-aocarrinho"> comprar</b>

											<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
													sprintf( '<a rel="nofollow" class="opcoes" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s"></a>',
														esc_url( $product->add_to_cart_url() ),
														esc_attr( isset( $quantity ) ? $quantity : 1 ),
														esc_attr( $product->id ),
														esc_attr( $product->get_sku() ),
														esc_attr( isset( $class ) ? $class : 'button' ),
														esc_html( $product->add_to_cart_text() )
													),
												$product );
											?>

									 	</button>
									</div>

								</div>
								<?php endwhile;wp_reset_query(); ?>


							</div>

						</div>



					</div>
				</div>
			</div>

	</div>