<?php
/**
 * adegamalbec functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package adegamalbec
 */

if ( ! function_exists( 'adegamalbec_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function adegamalbec_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on adegamalbec, use a find and replace
	 * to change 'adegamalbec' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'adegamalbec', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'adegamalbec' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'adegamalbec_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'adegamalbec_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function adegamalbec_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'adegamalbec_content_width', 640 );
}
add_action( 'after_setup_theme', 'adegamalbec_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function adegamalbec_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'adegamalbec' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'adegamalbec' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<strong class="widget-title">',
		'after_title'   => '</strong>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Mini Carrinho', 'adegamalbec' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'adegamalbec' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<strong class="widget-title">',
		'after_title'   => '</strong>',
	) );
}
add_action( 'widgets_init', 'adegamalbec_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function adegamalbec_scripts() {

	//FONT GOOGLE
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Pacifico|Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');

	//JAVA SCRIPT
	// wp_enqueue_script( 'adegamalbec-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );
	// wp_enqueue_script( 'adegamalbec-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js' );
	wp_enqueue_script( 'adegamalbec-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'adegamalbec-font-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	wp_enqueue_script( 'adegamalbec-jquery-facybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js' );
	wp_enqueue_script( 'adegamalbec-geral', get_template_directory_uri() . '/js/geral.js' );

	//CSS
	wp_enqueue_style( 'adegamalbec-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'adegamalbec-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style( 'adegamalbec-font-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'adegamalbec-jquery-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css');
	wp_enqueue_style( 'adegamalbec-style', get_stylesheet_uri() );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'adegamalbec_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
// CONFGURAÇÕES VIA REDUX
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_delimiter' );
function jk_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = ' &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp; ';
	return $defaults;
}
// BOTÃO ADICIONAR NO CARRINHO
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +

function woo_custom_cart_button_text() {

        return __( 'My Button Text', 'woocommerce' );

}

// SUPORTE AO WOOCOMMERCE
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// VERSIONAMENTO DE FOLHAS DE ESTILO
 function versionamentoEstilos($estilos){
 $estilos->default_version = "18102016";
 }
 add_action("wp_default_styles", "versionamentoEstilos");
 // VERSIONAMENTO DE SCRIPTS
 function versionamentoScripts($scripts){
 $scripts->default_version = "18102016";
 }
 add_action("wp_default_scripts", "versionamentoScripts");



// ADICIONANDO CAMPO NO FANTASIA
//1. Add a new form element...
add_action( 'register_form', 'nomeFantasia_register_form' );
function nomeFantasia_register_form() {

    $nomeFantasia = ( ! empty( $_POST['nomeFantasia'] ) ) ? trim( $_POST['nomeFantasia'] ) : '';

        ?>
        <div class="form-group" id="nomeFantasia" style="display:none">
            <label for="nomeFantasia"><?php _e( 'Nome Fantasia*', 'mydomain' ) ?></label>
            <input type="text" name="nomeFantasia"  id="nomeFantasia" class="input form-control" value="<?php echo esc_attr( wp_unslash( $nomeFantasia ) ); ?>" />
        </div>
        <?php
    }

    //2. Add validation. In this case, we make sure nomeFantasia is required.
    add_filter( 'registration_errors', 'nomeFantasia_registration_errors', 10, 3 );
    function nomeFantasia_registration_errors( $errors, $sanitized_user_login, $user_email ) {

        if ( empty( $_POST['nomeFantasia'] ) || ! empty( $_POST['nomeFantasia'] ) && trim( $_POST['nomeFantasia'] ) == '' ) {
            $errors->add( 'nomeFantasia_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
        }

        return $errors;
    }

    //3. Finally, save our extra registration user meta.
    add_action( 'user_register', 'nomeFantasia_user_register' );
    function nomeFantasia_user_register( $user_id ) {
        if ( ! empty( $_POST['nomeFantasia'] ) ) {
            update_user_meta( $user_id, 'nomeFantasia', trim( $_POST['nomeFantasia'] ) );
        }
    }

// UTILIZAR JQUERY VIA CDN DO GOOGLE NO WORDPRESS
function modify_jquery() {
   if(!is_admin()){
      wp_deregister_script('jquery');
      wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, '2.2.4');
      wp_enqueue_script('jquery');
   }
}


// MENU
add_action('init', 'modify_jquery');

function register_my_menus() {
register_nav_menus(
array(
'vinhosPorUvas' => __( 'Vinhos por uvas' ),

)
);
}
add_action( 'init', 'register_my_menus' );
