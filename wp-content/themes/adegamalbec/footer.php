<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adegamalbec
 */
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );

global $configuracao;

//FOTO ICON
$entrega = $configuracao['opt-info-entrega-icon']['url'];
$cartoes = $configuracao['opt-info-cartoes-icon']['url'];
$confiabilidade = $configuracao['opt-info-confiabilidade-icon']['url'];
$boleto = $configuracao['opt-info-boleto-icon']['url'];


?>
<!-- MESAGEM -->
	<?php

		$string1 = $configuracao['opt-info-frase'];
		$frase = explode(";", $string1);

	?>
	<div class="pg">
		<div class="container">
			<p class="mensagem-inicial"><?php echo $frase[0] ?><img src="<?php bloginfo('template_directory'); ?>/img/taca.png" alt=""><?php echo $frase[1] ?></p>
		</div>
	</div>



	<!-- INFORMAÇÕES DE PAGAMENTO -->
	<div class="info-de-pagamento">
		<ul class="form-pg">
			<li>
				<div class="row">
					<div class="col-md-3"><img src="<?php echo $entrega ?>" class="img-responsive" alt=""></div>
					<div class="col-md-9">
						<p>
							<b>Entrega</b>
							<?php echo $configuracao['opt-info-entrega']; ?>
						</p>
					</div>
				</div>
			</li>

			<li>
				<div class="row">
					<div class="col-md-3"><img src="<?php echo $cartoes ?>" class="img-responsive" alt=""></div>
					<div class="col-md-9">
						<p>
							<b>Cartões de Crédito</b>
							<?php echo $configuracao['opt-info-cartoes']; ?>
						</p>
					</div>
				</div>
			</li>

			<li>
				<div class="row">
					<div class="col-md-3"><img src="<?php echo $confiabilidade ?>" class="img-responsive" alt=""></div>
					<div class="col-md-9">
						<p>
							<b>Confiabilidade</b>
							<?php echo $configuracao['opt-info-confiabilidade']; ?>
						</p>
					</div>
				</div>
			</li>

			<li>
				<div class="row">
					<div class="col-md-3"><img src="<?php echo $boleto ?>" class="img-responsive" alt=""></div>
					<div class="col-md-9">
						<p>
							<b>Boleto</b>
							<?php echo $configuracao['opt-info-boleto']; ?>
						</p>
					</div>
				</div>
			</li>
		</ul>
	</div>

	<!-- RODAPÉ -->
	<footer class="rodape" style="display: ;">
		<div class="">

			<!-- NEWSLETTER -->
			<div class="newsletter row">
				<div class="info col-md-10 col-md-offset-1">

					<!-- INFORMAÇÕES -->
					<div class="row">
						<div class="col-md-2 text-right">
							<span>newsletter</span>
						</div>

						<div class="col-md-10 text-center">

							<p clas="info-informacoes">Entre com seu e-mail para receber informações </p>

							<!-- FORMULÁRIO -->
							<div class="info-newsletter">

								<!--START Scripts : this is the script part you can add to the header of your theme-->
								<script type="text/javascript" src="http://adegamalbec.pixd.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://adegamalbec.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://adegamalbec.pixd.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.1"></script>
								<script type="text/javascript" src="http://adegamalbec.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
								<script type="text/javascript">
								                /* <![CDATA[ */
								                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://adegamalbec.pixd.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
								                /* ]]> */
								                </script><script type="text/javascript" src="http://adegamalbec.pixd.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
								<!--END Scripts-->

								<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5723cff1ad4c6-3" class="wysija-msg ajax"></div><form id="form-wysija-html5723cff1ad4c6-3" method="post" action="#wysija" class="widget_wysija html_wysija">

								    <label class="hidden">Email <span class="wysija-required">*</span></label>

								    	<input type="text" name="wysija[user][email]" class=" validate[required,custom[email]]" title="Digite seu e-mail" placeholder="Digite seu e-mail" value="" />


								   <!--
								    <span class="abs-req">
								        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
								    </span> -->


								<input class="botao" type="submit" value="Enviar" />

								    <input type="hidden" name="form_id" value="3" />
								    <input type="hidden" name="action" value="save" />
								    <input type="hidden" name="controller" value="subscribers" />
								    <input type="hidden" value="1" name="wysija-page" />


								        <input type="hidden" name="wysija[user_list][list_ids]" value="3" />

								 </form></div>
							</div>
						</div>

					</div>

				</div>
			</div>

			<!-- MAPA E INFORMAÇÕES DO SITE -->
			<div class="mapa-site">
				<ul style="text-align:center">
					<!-- INFORMAÇÕES DE LOCALIZAÇÃO -->
					<li>
						<b>Adega Malbec Vinhos e Presentes</b>
						<span>CNPJ <?php echo $configuracao['opt-cnpj']; ?></span>
						<span>Telefone: <?php echo $configuracao['opt-contato-telefone']; ?></span>
						<?php

							$enderecoAdega = $configuracao['opt-endereco'];
							$enderecoGoogle = 'https://www.google.com.br/maps/place/' . urldecode($enderecoAdega);
						?>
						<span><a href="<?php echo $enderecoGoogle; ?>" title="Endereço da Adega Malbec no Google maps" target="_blank"><?php echo $enderecoAdega; ?></a></span>
						<span><?php echo $configuracao['opt-endereco-cidade']; ?></span>


						<a href="<?php echo $configuracao['opt-direcao-mapa']; ?>">Ver direções no mapa</a>
					</li>

					<!-- INFORMAÇÕES DE HORÁRIOS DE ATENDIMENTO -->
					<li>
						<b>Atendimento</b>
						<span>Seg-Sex</span>
						<span>Das <?php echo $configuracao['opt-horarioSegunda-rodape']; ?></span>
						<span>Sáb</span>
						<span>Das <?php echo $configuracao['opt-horarioSab-rodape']; ?></span>
					</li>

					<!-- MAPA DO SITE -->
					<li>
						<b>Empresa</b>
						<a href="<?php echo home_url('/sobre/'); ?>">Sobre</a>
						<a href="<?php echo home_url('loja/'); ?>">Loja</a>
						<!-- <a href="<?php echo home_url('blog/'); ?>">Blog Adega Malbec</a> -->
						<a href="<?php echo home_url('/politica-de-privacidade/'); ?>">Política de Privacidade</a>
						<a href="<?php echo home_url('/politica-de-entrega/'); ?>">Fretes e Entregas</a>
						<a href="<?php echo home_url('/contato/'); ?>">Fale Conosco</a>
					</li>

					<!-- INFORMAÇÕES DA CONTA-->
					<li>
						<b>Minha conta</b>
						<a href="<?php echo $urlMinhaConta ?>">Acesse sua conta</a>
						<a href="<?php echo home_url('minha-conta/'); ?>">Meus Pedidos</a>
						<a href="<?php echo $urlCarrinho  ?>">Minha Sacola</a>
					</li>

					 <script>(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src="//x.instagramfollowbutton.com/follow.js";s.parentNode.insertBefore(g,s);}(document,"script"));</script>

					<!-- REDES SOCIAIS -->
					<li>
						<b>+ Adega Malbec</b><!--
							<div style="display: block;" class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
							 <span class="ig-follow" data-id="5479dee" data-handle="igfbdotcom" data-count="true" data-size="small" data-username="true"></span> -->
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https://facebook.com/adegamalbec/&width=370&height=220&colorscheme=light&show_faces=true" scrolling="no" frameborder="0" allowtransparency="true" style="width: 100%; height: 220px; max-width: 380px; margin: 5px auto; border: 5px solid #fff; border-radius: 5px; overflow:hidden;"></iframe>
						<style>.ig-b- { display: inline-block; }
							.ig-b-v-24 { text-decoration:none;text-align: center; line-height: 23px; border-radius: 3px; border-top: 1px solid #7fa4bf; width: 175px; height: 24px; /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#4f7fa4+0,2f5283+100 */ background: #4f7fa4; /* Old browsers */ background: -moz-linear-gradient(top, #4f7fa4 0%, #2f5283 100%); /* FF3.6-15 */ background: -webkit-linear-gradient(top, #4f7fa4 0%,#2f5283 100%); /* Chrome10-25,Safari5.1-6 */ background: linear-gradient(to bottom, #4f7fa4 0%,#2f5283 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4f7fa4', endColorstr='#2f5283',GradientType=0 ); /* IE6-9 */}
							@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
								.ig-b-v-24 {  } }
						</style>
						<a href="https://www.instagram.com/adegamalbec/?ref=badge" class="ig-b- ig-b-v-24" target="_blank"><i class="fa fa-instagram"></i> Siga-nos no <b>Instagram</b></a>
					</li>

				</ul>
			</div>

			<!-- BANDERAS DE CARTÕES DE PAGAMENTOS -->
			<div class="cartoes-pagamento">
				<ul>
					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b1.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b2.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b3.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b6.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b4.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/b5.png" alt="">
					</li>

					<li>
						<img src="<?php bloginfo('template_directory'); ?>/img/6.png" alt="">
					</li>
				</ul>
			</div>

			<div class="copyright">
				<p>Adega Malbec - Todos os direitos reservados 2011 - 2016 <span>Se beber não dirija .Venda proibidas para menores de 18 anos.</span></p>
			</div>
		    <div class="col-md-12" style="text-align: center; background: #222528;">
		        <small>
		        	<span style="vertical-align: text-bottom;color:rgb(147, 147, 147);">Desenvolvido por</span>
		        	<a href="http://palupa.com.br/" target="_blank" title="Desenvolvido por Palupa Marketing" style="display: inline-block;"><img src="<?php echo get_template_directory_uri(); ?>/img/palupaLogo.png" style="max-height: 15px;"></a>
		        </small>
		    </div>

		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
