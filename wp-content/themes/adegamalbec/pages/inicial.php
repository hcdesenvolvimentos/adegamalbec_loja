<?php
/**
 * Template Name: Inicial
 * Description: Página de contato da Adega Malbec
 *
 * @package adegamalbec
 */
global $configuracao;
global $product;

get_header(); ?>

<!--PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		<div class="container">

			<!-- CARROSSEL DESTAQUE -->
			<div class="carrossel">
				<!-- BOTÕES DE NAVEGAÇÃO -->
				<div class="botoes-produtos">
					<button class="navegacaoDestqueFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
					<button class="navegacaoDestqueTras hidden-xs"><i class="fa fa-angle-right"></i></button>
				</div>
				<div id="carrossel-destaque" class="owl-Carousel">
					<?php
						// LOOP DE DESTAQUE
						$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

		                while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();

						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];

					?>
					<div class="item">
						<div class="bg-destaque" style="background: url(<?php echo $foto  ?>);">
							<!-- <p>
								Grandes
								<b>Vinhos</b>
								<span class="tipo-letraA">pequenos</span>
								<span class="tipo-letraB">preços</span>
							</p> -->
						</div>
					</div>
					<?php endwhile; wp_reset_query(); ?>

				</div>
			</div>

			<!-- CONTÚDO -->
			<div class="row">
				<!-- SIDEBAR  -->
				<div class="col-md-2">
					<?php include (TEMPLATEPATH . '/inc/sidebar-inicial.php'); ?>
				</div>

				<!-- CONTEÚDO LOJA -->
				<div class="col-md-10">

					<!--  INFORMAÇÕES-->
					<div class="info">
						<?php

							// LISTA DE CATEGORIAS
							// $arrayCategorias = array();
							// $categorias=get_categories($args);

							// foreach($categorias as $categoria) {
							// $arrayCategorias[$categoria->cat_ID] = $categoria->name;
							// $nomeCategoria = $arrayCategorias[$categoria->cat_ID];
							// $img = z_taxonomy_image_url($categoria->cat_ID);

						?>
						<a href="<?php echo $configuracao['opt-vinho-destaque-link'] ?>">
							<div class="conteudo-item" style="background: url(<?php echo $configuracao['opt-vinho-destaque']['url'] ?>);">
								<p></p>
								<span></span>
							</div>
						</a>

						<a href="<?php echo $configuracao['opt-receitas-link'] ?>">
							<div class="conteudo-item" style="background: url(<?php echo $configuracao['opt-receitas']['url'] ?>);">
								<p></p>
								<span></span>
							</div>
						</a>

						<a href="<?php echo $configuracao['opt-cervejasDestaque-link'] ?>">
							<div class="conteudo-item" style="background: url(<?php echo $configuracao['opt-cervejasDestaque']['url'] ?>);">
								<p></p>
								<span></span>
							</div>
						</a>

						<?php //} ?>
					</div>

					<!-- PRODUTOS EM DESTAQUE -->
					<div class="produto-destaque">


						<!-- BOTÕES DE NAVEGAÇÃO -->
						<div class="botoes-produtos">
							<span class="titulo-pg">produtos em destaque</span>
							<button class="navegacaoDestque-produtoFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
							<button class="navegacaoDestque-produtoTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						</div>

						<!-- PRODUTOS EM DESTAQUE -->
						<div class="carrossel-produtos-destaque" >
							<div id="carrossel-produtos-destaque" class="owl-Carousel">

								<!-- PRODUTO -->
								<?php
									$args =array( 'post_type' => 'product', 'product_cat' => 'destaques', 'posts_per_page' => 16, 'meta_value' => 'yes' );
						            $loop = new WP_Query( $args );
						            while ( $loop->have_posts() ) : $loop->the_post();

								?>

								<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">
												<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

											<?php
											if($product->product_type == "variable"){
												$prodVars = $product->get_available_variations();
												foreach ($prodVars as $prodVar) {

													if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
														$unId = $prodVar['variation_id'];
														echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
													}
												}
											}else{
												$unId = $product->id;
												if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
													echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
												}
											}

											?>


										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h2><?php the_title(); ?></h2>
									</div>

									<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo'
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>
									<!-- PREÇO -->
									<p class="preco">R$ <?php echo number_format($product->price,2,",","." ); ?></p>
									<?php
										$price   = $product->get_price();
										$price3x = $price / 3;
										$price2x = $price / 2;
									?>

									<?php if ($price >= 120): ?>
									<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
									<?php elseif($price >= 80): ?>
									<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
									<?php else: ?>
									<p class="promocao" style="height: 20px;"></p>
									<?php endif; ?>

									<?php
									if($product->product_type == "variable"){
										$prodVars = $product->get_available_variations();
										foreach ($prodVars as $prodVar) {
											if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
												$sixId = $prodVar['variation_id'];
												$str = $prodVar["attributes"]["attribute_pa_quantidade"];
												$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);

												$sixDiv = $prodVar['display_price'] / $qTD;

												$sixDiv = money_format('%.2n', $sixDiv);

												echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>6un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

											} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
												echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
											}
										}
									}else{
										$quantidadeProduto = $product->get_stock_quantity();
										if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
											echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
										}
									}

									?>

								</div>
								<?php endwhile;wp_reset_query(); ?>

							</div>
						</div>
					</div>

					<div class="produto-destaque">


					<!-- BOTÕES DE NAVEGAÇÃO -->
						<div class="botoes-produtos">
							<span class="titulo-pg">novidades da adega</span>
							<button class="navegacaoDestque-novidadesFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
							<button class="navegacaoDestque-novidadesTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						</div>

						<!-- PRODUTOS EM DESTAQUE -->
						<div class="carrossel-produtos-novidades" >
							<div id="carrossel-produtos-novidades" class="owl-Carousel">

								<!-- PRODUTO -->
								<?php
									$args =array( 'post_type' => 'product', 'product_cat' => 'novidades', 'posts_per_page' => 16, 'meta_value' => 'yes' );
						            $loop = new WP_Query( $args );
						            while ( $loop->have_posts() ) : $loop->the_post();

								?>
								<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto inicial">
												<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

											<?php
											if($product->product_type == "variable"){
												$prodVars = $product->get_available_variations();
												foreach ($prodVars as $prodVar) {

													if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
														$unId = $prodVar['variation_id'];
														echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
													}
												}
											}else{
												$unId = $product->id;
												if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
													echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
												}
											}

											?>


										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h2><?php the_title(); ?></h2>
									</div>

									<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo'
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>

									<!-- PREÇO -->
									<p class="preco">R$ <?php echo number_format($product->price,2,",","." ); ?></p>
									<?php
										$price   = $product->get_price();
										$price3x = $price / 3;
										$price2x = $price / 2;
									?>

									<?php if ($price >= 120): ?>
									<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
									<?php elseif($price >= 80): ?>
									<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
									<?php else: ?>
									<p class="promocao" style="height: 20px;"></p>
									<?php endif; ?>

									<?php
									if($product->product_type == "variable"){
										$prodVars = $product->get_available_variations();
										foreach ($prodVars as $prodVar) {
											if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
												$sixId = $prodVar['variation_id'];
												$str = $prodVar["attributes"]["attribute_pa_quantidade"];
												$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);

												$sixDiv = $prodVar['display_price'] / $qTD;

												$sixDiv = money_format('%.2n', $sixDiv);

												echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>6un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

											} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
												echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
											}
										}
									}else{
										$quantidadeProduto = $product->get_stock_quantity();
										if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
											echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
										}
									}

									?>
								</div>
								<?php endwhile;wp_reset_query(); ?>

							</div>
						</div>
					</div>

					<div class="produto-destaque">


					<!-- BOTÕES DE NAVEGAÇÃO -->
						<div class="botoes-produtos">
							<span class="titulo-pg">produtos mais vendidos</span>
							<button class="navegacaoDestque-vendidosFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
							<button class="navegacaoDestque-vendidosTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						</div>

						<!-- PRODUTOS EM DESTAQUE -->
						<div class="carrossel-produtos-novidades" >
							<div id="carrossel-produtos-vendidos" class="owl-Carousel">

								<!-- PRODUTO -->
								<?php
									$args =array( 'post_type' => 'product', 'posts_per_page' => 10, 'meta_key' => 'total_sales', 'orderby' => 'meta_value_num', );
						            $loop = new WP_Query( $args );
						            while ( $loop->have_posts() ) : $loop->the_post();

								?>
								<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">
												<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>


											<?php
											if($product->product_type == "variable"){
												$prodVars = $product->get_available_variations();
												foreach ($prodVars as $prodVar) {

													if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
														$unId = $prodVar['variation_id'];
														echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
													}
												}
											}else{
												$unId = $product->id;
												if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
													echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"></a>';
												}
											}


											?>


										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h2><?php the_title(); ?></h2>
									</div>

									<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo'
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo'
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>

									<!-- PREÇO -->
									<p class="preco">R$ <?php echo number_format((float)$product->get_price(),2, ',', '.'); ?></p>
									<?php
										$price   = $product->get_price();
										$price3x = $price / 3;
										$price2x = $price / 2;
									?>

									<?php if ($price >= 120): ?>
									<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
									<?php elseif($price >= 80): ?>
									<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
									<?php else: ?>
									<p class="promocao" style="height: 20px;"></p>
									<?php endif; ?>

									<?php
									if($product->product_type == "variable"){
										$prodVars = $product->get_available_variations();
										foreach ($prodVars as $prodVar) {
											if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
												$sixId = $prodVar['variation_id'];
												$str = $prodVar["attributes"]["attribute_pa_quantidade"];
												$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);

												$sixDiv = $prodVar['display_price'] / $qTD;

												$sixDiv = money_format('%.2n', $sixDiv);

												echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>6un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

											} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
												echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
											}
										}
									}else{
										$quantidadeProduto = $product->get_stock_quantity();
										if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
											echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
										}
									}

									?>

								</div>
								<?php endwhile;wp_reset_query(); ?>

							</div>
						</div>
					</div>


					<!-- CARROSSEL DE PAÍSES -->
					<div class="carrossel-paises">

					<div class="internas">
						<span class="titulo"><?=$configuracao['opt-info-titulo-carrossel']?></span>
					</div>
						<!-- BOTÕES DE NAVEGAÇÃO -->
						<div class="botoes-produtosingle">
							<button class="navegacaoDestque-novidades-paisesFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
							<button class="navegacaoDestque-novidades-paisesTras hidden-xs"><i class="fa fa-angle-right"></i></button>
						</div>
						<div id="carrossel-paises" class="owl-Carousel">
						<?php
							$harmonizacao = $configuracao['opt-harmonizacao'];
							foreach ($harmonizacao as $harmonizacao):
								$item = explode("|",$harmonizacao);


						 ?>

							<div class="item">
								<a href="<?php echo $item[1]  ?>">
									<div class="conteudo-pais">
										<img src="<?php echo $item[0]  ?>" alt="">
									</div>
								</a>
							</div>
							<?php endforeach; ?>
						</div>



					</div>



				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
<script>
	var sacolaURL = "<?php bloginfo('template_directory'); ?>";
	$('a.areaImgComprar').append('<img src="'+sacolaURL+'/img/sacola.png" />');
	$('a.areaImgComprar').append("<span>Adicionar ao carrinho</span>");

</script>