<?php
/**
 * Template Name: Fale Conosco
 * Description: Página de contato da Adega Malbec
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>

<div class="pg pg-empresa">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar">
					<span><b>Institucional</b></span>
					<a href="<?php echo home_url('/sobre-adega-malbec/'); ?>">Sobre a Adega Malbec </a>
					<a href="<?php echo home_url('/empresa-e-eventos/'); ?>">Para Empresa & Eventos </a>
					<a href="<?php echo home_url('/politica-de-entrega/'); ?>">Política de Entrega </a>
					<a href="<?php echo home_url('/politica-de-privacidade/'); ?>">Política de Privacidade </a>
					<a href="<?php echo home_url('/contato/'); ?>">Fale Conosco </a>
				</div>
			</div>
			<div class="col-md-10">
				<div class="conteudo-empresa">
					<div class="subtitulo">
						<span>Entre em contato</span>
					</div>

					<div class="info-contato">
						<p class="info"><?php the_content(); ?></p>
						<h4>Adega Malbec <span>- Loja Física</span> </h4>
						<p class="contato"><strong>Telefone</strong> <span><?php echo $configuracao['opt-contato-telefone']; ?></span> </p>
						<p class="contato"><strong>E-mail</strong> <span><?php echo $configuracao['opt-contato-email']; ?></span> </p>
						<?php

							$enderecoAdega = $configuracao['opt-endereco'];
							$enderecoGoogle = 'https://www.google.com.br/maps/place/' . urldecode($enderecoAdega);
						?>
						<p class="contato"><strong>Endereço</strong> <span><?php echo $configuracao['opt-endereco']; ?> <?php echo $configuracao['opt-endereco-cidade']; ?> <a href="<?php echo $enderecoGoogle; ?>" title="Endereço da Adega Malbec no Google maps" target="_blank">Ver no mapa</a></span> </p>
						<p class="contato"><strong>Atendimento</strong> <span>de Seg à Sex - das <?php echo $configuracao['opt-horarioSegunda-rodape']; ?> <span>aos Sáb - das <?php echo $configuracao['opt-horarioSab-rodape']; ?></span></span> </p>

						<div class="form-contat">
							<?php
	                           echo do_shortcode('[contact-form-7 id="6" title="Contato Fale Conosco"]');
	                        ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>