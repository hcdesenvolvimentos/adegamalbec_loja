<?php
/**
 * Template Name: Página de ajuda
 * Description: 
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>
<?php if ( have_posts() ) : while( have_posts() ) : the_post();

       	// LOOP DE CLIENTE	
		
		$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$foto = $foto[0];
        
    endwhile; endif; ?>
<div class="pg pg-empresa">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<div class="conteudo-empresa">
					<div class="bg-empresa" style="background: url('<?php echo $foto ?>');">
						<span class="titulo-ajuda"><?php echo get_the_title() ?></span>
					</div>

					<p><?php the_content(); ?></p>
					
					<div class="subtitulo">
						<span> Entre em contato </span>
					</div>
					
					<div class="info-contato">
						
						<h4>Adega Malbec <span>- Loja Física</span></h4>
						<p class="contato">Telefone <span> <?php echo $configuracao['opt-contato-telefone']; ?></span> </p>
						<p class="contato">E-mail <span> <?php echo $configuracao['opt-contato-email']; ?></span> </p>

						<div class="form-contat">
							<!-- <?php
	                           echo do_shortcode('[contact-form-7 id="19" title="Contato Empresa e Eventos"]');
	                        ?> -->
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<?php get_footer(); ?>