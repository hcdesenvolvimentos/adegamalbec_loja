<?php
/**
 * Template Name: Produtos em destaque
 * Description: Página de contato da Adega Malbec
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>

<!-- PÁGINA LOJA  -->
	<div class="pg pg-loja" style="display:;">
		<div class="container">
			<div class="row">
				<!-- SIDEBAR -->
				<div class="col-md-2">
					<!-- LOJA -->
					<?php include (TEMPLATEPATH . '/inc/menu-lateral.php'); ?>
				
				</div>

				<!-- CONTEÚDO LOJA  -->
				<!-- CONTEÚDO LOJA  -->
				<div class="col-md-10">
					<div class="conteudo-loja">
						
						<!-- FOTO TOPO LOJA -->
						<div class="bg-loja" style="background:url(<?php echo $configuracao['opt-info-foto-loja']['url'];  ?>);">
							<!-- <p>Vinhos</p>
							<span>Chilenos</span> -->
						</div>
						
						<!-- DESCRIÇÃO  -->
						<p class="descricao-loja page-description"><?php echo $configuracao['opt-info-frase-pagina'];  ?> </p>
						
						<!-- FILTRO CONTEÚDO DA LOJA  -->
						<div class="filtroConteudo-loja">						
							<div class="row">
								
								<div class="col-md-6">								

									<!-- <p>1-16 de 78 produtos relacionados</p>	 -->
									<div class="form-group resultados">
										<?php
											/**
											 * woocommerce_before_shop_loop hook.
											 *
											 * @hooked woocommerce_result_count - 20
											 * @hooked woocommerce_catalog_ordering - 30
											 */
											do_action( 'woocommerce_before_shop_loop' );
										?>
									</div>								
								</div>
								
								<div class="col-md-6 text-right">
									<!-- FORMA DE VIZUALIZAÇÃO  -->
									<div id="lista" class="icon"><i class="fa fa-th-list" aria-hidden="true"></i></div>
									<div id="grade" class="icon"><i class="fa fa-th-large" aria-hidden="true"></i></div>
								
									<div class="form-group select">
										<?php
											/**
											 * woocommerce_before_shop_loop hook.
											 *
											 * @hooked woocommerce_result_count - 20
											 * @hooked woocommerce_catalog_ordering - 30
											 */
											do_action( 'woocommerce_before_shop_loop' );
										?>
									</div>
									
								</div>
							
							</div>					
						</div>

						<!-- PRODUTOS DA LOJA -->
						<div class="produtos-loja">			
							<!-- PRODUTOS EM DESTAQUE -->
							<div class="carrossel-produtos-destaque" >
							   <style>
								.foto-produto  .product_type_variable{
									display: none!important;
								}
							  </style>
							   <?php 
										echo do_shortcode('[recent_products]');
							    ?>							
										
													
							</div>	
							
						</div>

						<!-- <div class="paginador-loja">
							<ul>
								<li><a href="">1</a></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
								<li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
							</ul>
						</div>									
 -->
					</div>
				</div>
			</div>
		</div>
	</div>

	
     
 
<?php get_footer(); ?>