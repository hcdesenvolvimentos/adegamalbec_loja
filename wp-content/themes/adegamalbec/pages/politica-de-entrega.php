<?php
/**
 * Template Name: Política de Entrega
 * Description:
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>

<div class="pg pg-empresa politica-deentrega">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="sidebar">
					<span><b>Institucional</b></span>
					<a href="<?php echo home_url('sobre-adega/'); ?>">Sobre a Adega Malbec</a>
					<a href="<?php echo home_url('/empresa-e-eventos/'); ?>">Para Empresa & Eventos </a>
					<a href="<?php echo home_url('/politica-de-entrega/'); ?>">Política de Entrega </a>
					<a href="<?php echo home_url('/politica-de-privacidade/'); ?>">Política de Privacidade </a>
					<a href="<?php echo home_url('/contato/'); ?>">Fale Conosco </a>
				</div>
			</div>
			<div class="col-md-9">
				<div class="conteudo-empresa">
					<div class="subtitulo">
						<span><?php the_title(); ?></span>
					</div>
					
					<?php the_content(); ?>

				</div>
			</div>
		</div>
	</div>	
</div>

<?php get_footer(); ?>