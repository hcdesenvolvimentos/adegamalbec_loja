<?php
/**
 * Template Name: Sobre Adega Malbec
 * Description: 
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>

<div class="pg pg-empresa">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar">
					<span><b>Institucional</b></span>
					<a href="<?php echo home_url('/sobre-adega-malbec/'); ?>">Sobre a Adega Malbec</a>
					<a href="<?php echo home_url('/empresa-e-eventos/'); ?>">Para Empresa & Eventos </a>
					<a href="<?php echo home_url('/politica-de-entrega/'); ?>">Política de Entrega </a>
					<a href="<?php echo home_url('/politica-de-privacidade/'); ?>">Política de Privacidade </a>
					<a href="<?php echo home_url('/contato/'); ?>">Fale Conosco </a>
				</div>
			</div>
			<div class="col-md-10">
				<div class="conteudo-empresa">
					<?php 
					$img  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
		        	$img  = $img[0];
				 ?>
					<div class="bg-empresa" style="background: url(<?php echo $img ?>);">
						<span><?php echo  get_the_title() ?></span>
					</div>

					<p><?php the_content(); ?></p>
					
					
					
					
						
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<?php get_footer(); ?>