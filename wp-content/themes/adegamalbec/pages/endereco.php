<?php
/**
 * Template Name: Endereços
 * Description: Página de endereços
 *
 * @package adegamalbec
 */
global $configuracao;

get_header(); ?>
	<!-- PÁGINA DE DADOS CADASTRADO -->
	<div class="pg pg-dados-cadastrados internas" style="display: ;">
		<div class="container">
			
			<!-- DADOS CADASTRAIS -->
			<div class="dados">
				<span class="titulo">meu cadastro</span>

				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3 side">
						<div class="sidebar-cadastro">
							<div class="foto-perfil"><img src="img/user.png" alt=""></div>
							<span>Lucas</span>
							<?php 

									
							printf(
								__( ' <a href="%2$s">Sair</a>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);
							 ?>
							

							<div class="menu">
								<a href="<?php echo home_url('/minha-conta/edit-account/'); ?>">Meus dados cadastrais</a>
								<a href="<?php echo home_url('/minha-conta/edit-address/entrega/'); ?>">Meus endereços</a>
								
								<a href="<?php echo home_url('/'); ?>">Meus pedidos</a>
								<a href="<?php echo home_url('/'); ?>">Minhas avaliações</a>
							</div>
						</div>
					</div>

					<!-- FORMULÁRIO CADASTRO -->
					<style>
						.info{
							display: block;
							margin-top: 50px;
						}
						.info a{
							display: block;

						}
						.nome{
							display: block;
							text-align: center;
						}
					</style>
					<div class="col-md-9">
						
						<p class="myaccount_user">
							<?php
							printf(
								__( '<span class="nome">Olá  <strong>%1$s</strong> (não é %1$s? <a href="%2$s">Sair</a>).</span>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);

							printf( __( '<span class="info">No painel da sua conta você pode ver seus pedidos recentes, gerenciar seus endereços de entrega e cobrança <a href="%s">editar sua senha e detalhes da conta.</a>.</span>', 'woocommerce' ),
								wc_customer_edit_account_url()
							); 	
							?>
						</p>

						 <?php do_action( 'woocommerce_before_my_account' ); ?>

						<?php wc_get_template( 'myaccount/my-downloads.php' ); ?>

						<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

						<?php wc_get_template( 'myaccount/my-address.php' ); ?>

						<?php do_action( 'woocommerce_after_my_account' ); ?> 
							
					</div>

				</div>
			</div>
		</div>
	</div>
	

<?php get_footer(); ?>