<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package adegamalbec
 */

get_header(); ?>

	<!-- BLOG -->
	<div class="pg pg-blog" style="display:; ">
		
		<div class="container">	
				

			<div class="row">
				<div class="col-md-12">
				<?php 
								// LOOP DE DESTAQUE
					// $destaquesPost = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	                
	    //             while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();
					$the_query = new WP_Query( 'posts_per_page=-1' ); 

						 while ($the_query -> have_posts()) : $the_query -> the_post();
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];
					if(function_exists('the_ratings')) { the_ratings(); }
				?>
					<div class="post">
						<!-- TÍTULO DO POST -->
						<div class="titulo-post">
							<a href="<?php echo get_permalink(); ?>"><p>"<?php echo get_the_title() ?> "</p></a>
							<span><!-- 6 comentários --></span>
						</div>
						<!-- DATA -->
						<span class="data">
							 <?php the_time('j \d\e F \d\e Y') ?>
							 - <?php the_time('g') ?> HORAS A TRÁS
						</span>
						<!-- IMAGEM -->
						<div class="bg-post" style="background: url(<?php echo $foto  ?>) no-repeat;"></div>
						<!-- DESCRIÇÃO -->
						<p class="descricao">
							<?php
								$content = get_the_content();
								$conteudo = substr($content, 0, 300).'...';
								echo $conteudo;
							?>
						</p>
						<!-- LINK CONTINUE LENDO -->
						<a href="<?php echo get_permalink(); ?>">continuar lendo</a>
					
					</div>
				<?php endwhile; wp_reset_query(); ?>	
				</div>


				
			</div>

			<div class="paginador">
				
				<?php if (function_exists("pagination")) {
			    pagination();
				} 
				?>
			</div>
		</div>
	</div>

<?php

get_footer();
