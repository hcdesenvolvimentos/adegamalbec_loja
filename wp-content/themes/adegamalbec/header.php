<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package adegamalbec
 */
global $woocommerce;
global $configuracao;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );

$cart_subtotal = $woocommerce->cart->get_cart_subtotal();

global $configuracao;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="loader"></div>
<div class="modal" id="modalCartSuccess" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content modalCartSuccess">
			<i class="fa fa-shopping-cart"></i>
			<i class="fa fa-plus"></i>
			<strong>Sucesso</strong>
			<p>Produto adicionado no carrinho com sucesso!</p>
			<a href="<?php echo $urlCarrinho; ?>">Ir para o carrinho</a>
			<button id="modalCartSuccessFechar" class="btn btn-default" data-dismiss="modalCartSuccess">Fechar</button>
		</div>
	</div>
</div>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>
		(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<?php
	global $woocommerce;
	// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
	$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
	$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';

?>

<!-- IMAGENS DAS PAIS CATEGORIAS DO MENU / RECUPERANDO POR REDUX-->
<?php
	$Imgcategoryvinho = $configuracao['opt-vinho']['url'];
	$Imgcategoryespumante = $configuracao['opt-espumante']['url'];
	$Imgcategorycervejas = $configuracao['opt-cervejas']['url'];
	$Imgcategorycestas_presentes = $configuracao['opt-cestas-Presenter']['url'];
	$Imgcategoryacessorios_utilidades = $configuracao['opt-acessorios-Utilidades']['url'];

		// LINKS
	$ImgcategoryvinholLink = $configuracao['opt-vinho-link'];
	$ImgcategoryespumantelLink = $configuracao['opt-espumante-link'];
	$ImgcategorycervejaslLink = $configuracao['opt-cervejas-link'];
	$Imgcategorycestas_presenteslLink = $configuracao['opt-Presenter-link'];
	$Imgcategoryacessorios_utilidadeslLink = $configuracao['opt-acessorios-link'];
 ?>
<!-- HEADER -->
	<header class="topo">

		<div class="area-topo">
			<div class="container">
				<div class="row ">
					<div class="col-md-2">
						<!--LOGO  -->
						<a href="<?php echo home_url('/'); ?>" class="link-logo" title="Adega Malbec">
							<h1>Adega Malbec</h1>
						</a>
					</div>
					<div class="col-md-10">
						<ul class="ulTopo">
							<li class="liAreaPesquisa">
								<!-- ÁREA DE PESQUISA -->
								<div class="area-pesquisa">
									<input class="pesquisar" type="submit" id="yith-searchsubmit" >
									 <?php  echo do_shortcode('[yith_woocommerce_ajax_search]'); ?>
								</div>
							</li>
							<li>
								<!-- ÁREA INFO CONTA -->
								<div class="area-info-conta">
									<?php if (is_user_logged_in() == true) { ?>
									<a href="<?php echo $urlMinhaConta ?>" style="margin:0;    font-weight: inherit;"><span>Bem vindo(a), <?php echo $current_user->user_login; ?>.</span></a>
									<?php } else{ ?>
									<a href="" data-toggle="modal" data-target=".bs-example-modal-sm" >Acesse sua conta</a>
									<?php }  ?>
									<a href="<?php echo home_url('pagina-de-ajuda'); ?>">Ajuda</a>
									<?php if (is_user_logged_in() == true) { ?>
									<a href="<?php echo $urlLogout ?>" id="sair">Sair</a>
									<?php } ?>
								</div>
							</li>
							<li class="liAreaCarrinho">
								<!-- ÁREA  CARRINHO -->
								<div class="area-carrinho">
									<div class="menu-minhaconta">
									<img src="<?php bloginfo('template_directory'); ?>/img/icon.png" alt="">
										<a href="<?php echo $urlCarrinho ?>">Minha Sacola <?php echo $cart_subtotal ?><i class="fa fa-angle-down"></i></a>
									</div>
									<span class="qtd-header" id="qtd-header" ><?php echo $qtdItensCarrinhoRotulo;?></span>
								</div>
								<div class="informacoes-carrinho">
									<?php dynamic_sidebar( 'sidebar-2' ); ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- MENU  -->
		<div class="navbar" role="navigation">
				<div class="container">
				<!-- MENU MOBILE TRIGGER -->
				<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<!--  MENU MOBILE-->
				<div class="row navbar-header">

					<nav class="collapse navbar-collapse" id="collapse">

						<!-- <ul class="nav navbar-nav">
							<?php

								// LISTAR CATEGORIAS PAI
						        $argsCategorias = array(
										                'taxonomy'     => 'product_cat',
										                'child_of'     => 0,
										                'parent'       => 0,
										                'orderby'      => 'name',
										                'pad_counts'   => 0,
										                'hierarchical' => 1,
										                'title_li'     => '',
										                'hide_empty'   => 0
										        	);

						        $listaCategorias = get_categories($argsCategorias);

											            if($listaCategorias) {

											                foreach($listaCategorias as $categoria):
									                			if($categoria->category_nicename == "vinhos" || $categoria->category_nicename == "espumantes" || $categoria->category_nicename == "cervejas" || $categoria->category_nicename == "cestas-presentes" || $categoria->category_nicename == "acessorios-utilidades"):
									               	?>

							<li class="dropdown">

								<a href="<?php echo get_term_link($categoria->slug, 'product_cat') ?>" class="dropdown-toggle" ><?php echo $categoria->name  ?></a>

								LISTAR  SUB CATEGORIAS
								<?php

									$categoriaId = $categoria->term_id;
							        $argsSubCategorias = array(
											                'taxonomy'     => 'product_cat',
											                'child_of'     => 0,
											                'parent'       => $categoriaId ,
											                'orderby'      => 'name',
											                'pad_counts'   => 0,
											                'hierarchical' => 1,
											                'title_li'     => '',
											                'hide_empty'   => 0
											        	);

							        $listaSubCategorias = get_categories($argsSubCategorias);
							      if ($categoria->name == "Vinhos"):
							      	# code...

						        ?>

								<ul class="dropdown-menu">
									<?php

										$attribute_taxonomies = wc_get_attribute_taxonomies();
										$taxonomy_terms = array();

										if ( $attribute_taxonomies ) :
										    foreach ($attribute_taxonomies as $tax) :
										    if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))) :
										        $taxonomy_terms[$tax->attribute_name] = get_terms( wc_attribute_taxonomy_name($tax->attribute_name), 'orderby=name&hide_empty=0' );

									    	// var_dump($taxonomy_terms['pais']);
									    	// var_dump($taxonomy_terms['tipo']);
									    	// var_dump($taxonomy_terms['uvas']);
							    	?>


								   <?php
									   	endif;
										endforeach;
										endif;
									?>

										<li>

											<small><?php echo $categoria->name  ?> por País</small>
											<?php
												foreach ($taxonomy_terms['pais'] as $taxonomy_terms['pais']):

												$nomePais = $taxonomy_terms['pais'];

											?>

												<a href="<?php echo "http://adegamalbec.pixd.com.br/categoria-produto/vinhos?filter_pais=".$nomePais->term_id ?>"><?php echo $nomePais->name ?></a>

											<?php endforeach; ?>
											<a href="<?php echo get_term_link($categoria->slug, 'product_cat') ?>" class="link-ver-todos">  + Ver todos </a>

										</li>

										<li>

											<small><?php echo $categoria->name  ?> por Tipo</small>
											<?php
												foreach ($taxonomy_terms['tipo'] as $taxonomy_terms['tipo']):

												$nomeTipo = $taxonomy_terms['tipo'];
											?>
												<a href="<?php echo "http://adegamalbec.pixd.com.br/categoria-produto/vinhos?filter_tipo=".$nomeTipo->term_id ?>"><?php echo $nomeTipo->name ?></a>
											<?php endforeach; ?>


										</li>

										<li>

											<small><?php echo $categoria->name  ?> por Uvas</small>
											<?php
												foreach ($taxonomy_terms['uvas'] as $taxonomy_terms['uvas']):

												$nomeUvas = $taxonomy_terms['uvas'];
											?>
												<a href="<?php echo "http://adegamalbec.pixd.com.br/categoria-produto/vinhos?filter_uvas=".$nomeUvas->term_id ?>"><?php echo $nomeUvas->name ?></a>
											<?php endforeach; ?>


										</li>




									VERIFICANDO CATEGORIA PAI
									<?php
										$nome_categoria = $categoria->name;

										if ($categoria->name == "Vinhos") {
											$fotoCaterory = $Imgcategoryvinho;
											$fotoCateroryLink = $ImgcategoryvinholLink;

										}elseif ($categoria->name == "Espumantes") {
											$fotoCaterory = $Imgcategoryespumante;
											$fotoCateroryLink = $ImgcategoryespumanteLink;
										}elseif ($categoria->name == "Cervejas") {
											$fotoCaterory = $Imgcategorycervejas;
											$fotoCateroryLink = $ImgcategorycervejaslLink;
										}elseif ($categoria->name == "Cestas & Presentes") {
											$fotoCaterory = $Imgcategorycestas_presentes;
											$fotoCateroryLink = $Imgcategorycestas_presentesLink;
										}elseif ($categoria->name == "Acessórios & Utilidades") {
											$fotoCaterory = $Imgcategoryacessorios_utilidades;
											$fotoCateroryLink = $Imgcategoryacessorios_utilidadesLink;
										}
									 ?>
										<li class="foto">

											<a href="<?php echo $fotoCateroryLink ?>">
												<img src="<?php echo $fotoCaterory ?>" class="img-responsive" alt="">
											</a>
										</li>
								</ul>


							</li>
							 <?php endif; endif;
										                endforeach; }
							?>
							<li><a href="<?php echo home_url('/empresa-e-eventos/'); ?>">Para Empresas & Eventos</a></li>
							<li><a href="<?php echo home_url('promocoes/'); ?>"><span class="circulo-promocao"></span>Promoções</a></li>
							<li><a href="<?php echo home_url('recentes/'); ?>"><span class="lancamentos"></span>Lançamentos</a></li>

						</ul> -->
						<div style="display: none;">

							<ul id="megamenuVinhos" class="dropdown-menu">
								<?php

								$attribute_taxonomies = wc_get_attribute_taxonomies();
								$taxonomy_terms = array();

								if ( $attribute_taxonomies ) {
									foreach ($attribute_taxonomies as $tax) {
										if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))){
											$taxonomy_terms[$tax->attribute_name] = get_terms( wc_attribute_taxonomy_name($tax->attribute_name), 'orderby=name&hide_empty=0' );
										}
									}
								}
								?>

								<li>

									<small>Vinhos por País</small>
									<?php
									foreach ($taxonomy_terms['pais'] as $taxonomy_terms['pais']):

										$nomePais = $taxonomy_terms['pais'];

									?>

									<a href="<?php echo "http://adegamalbec.pixd.com.br/categoria-produto/vinhos?filter_pais=".$nomePais->term_id ?>"><?php echo $nomePais->name ?></a>

									<?php endforeach; ?>

									<a href="<?php echo get_term_link($categoria->slug, 'product_cat') ?>" class="link-ver-todos">  + Ver todos </a>

								</li>

								<li>

									<small>Vinhos por Tipo</small>
									<?php
									foreach ($taxonomy_terms['tipo'] as $taxonomy_terms['tipo']):

										$nomeTipo = $taxonomy_terms['tipo'];

									?>
									<a href="<?php echo "http://adegamalbec.pixd.com.br/categoria-produto/vinhos?filter_tipo=".$nomeTipo->term_id ?>"><?php echo $nomeTipo->name ?></a>
									<?php endforeach; ?>

								</li>

								<li>

										<small>Vinhos por Uvas</small>

										<?php 
										// var_dump($vinhosPoruvas);
											$vinhosPorUvas = array(
												'theme_location'  => '',
												'menu'            => 'Vinhos por uvas',
												'container'       => false,
												'container_class' => '',
												'container_id'    => '',
												'menu_class'      => '',
												'menu_id'         => '',
												'echo'            => false,
												'fallback_cb'     => '',
												'before'          => '',
												'after'           => '',
												'link_before'     => '',
												'link_after'      => '',
												'items_wrap'      => '%3$s',
												'depth'           => 0,
												'walker'          => ''
												);
											echo strip_tags(wp_nav_menu( $vinhosPorUvas ), '<a>' );
											//wp_nav_menu( $vinhosPorUvas );
										 ?>
						
								</li> 




								<!-- VERIFICANDO CATEGORIA PAI -->
								<?php
								$nome_categoria = $categoria->name;

								if ($categoria->name == "Vinhos") {
									$fotoCaterory = $Imgcategoryvinho;
									$fotoCateroryLink = $ImgcategoryvinholLink;

								}elseif ($categoria->name == "Espumantes") {
									$fotoCaterory = $Imgcategoryespumante;
									$fotoCateroryLink = $ImgcategoryespumanteLink;
								}elseif ($categoria->name == "Cervejas") {
									$fotoCaterory = $Imgcategorycervejas;
									$fotoCateroryLink = $ImgcategorycervejaslLink;
								}elseif ($categoria->name == "Cestas & Presentes") {
									$fotoCaterory = $Imgcategorycestas_presentes;
									$fotoCateroryLink = $Imgcategorycestas_presentesLink;
								}elseif ($categoria->name == "Acessórios & Utilidades") {
									$fotoCaterory = $Imgcategoryacessorios_utilidades;
									$fotoCateroryLink = $Imgcategoryacessorios_utilidadesLink;
								}
								?>
								<li class="foto">

									<a href="<?php echo $fotoCateroryLink ?>">
										<img src="<?php echo $fotoCaterory ?>" class="img-responsive" alt="">
									</a>
								</li>
							</ul>

						</div>

						<?php
						$defaults = array(
							'theme_location'  => '',
							'menu'            => '',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'nav navbar-nav',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 2,
							'walker'          => ''
							);
						wp_nav_menu( $defaults );
						?>



					</nav>

				</div>

			</div>

		</div>


		<!-- MODAL DE LOGIN -->
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			    <div class="modal-body">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

			      	<div class="login">
			      		<span>Login</span>
			      		<p>Entre para ter acesso a histórico de compras, cadastro e muito mais</p>
			      		<a href="http://adegamalbec.pixd.com.br/wp-login.php?ywsl_social=facebook&amp;redirect=http%3A%2F%2Fadegamalbec.pixd.com.br%2Fminha-conta%2F" class="face"><i class="fa fa-facebook" aria-hidden="true"></i>Entrar com facebook</a>
			      		<p>Ou entre com seu email ou usuário</p>
			      		<div class="form">
							<!-- FORMULÁRIO DE LOGIN  -->
		      				<form method="post" class="">

								<?php do_action( 'woocommerce_login_form_start' ); ?>
									<!-- LOGIN -EMAIL -->
									<input type="text" placeholder="E-mail ou usuário" class="" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />

									<!-- SENHA -->
									<input class="" placeholder="senha" type="password" name="password" id="password" />

								<?php do_action( 'woocommerce_login_form' ); ?>


								<?php wp_nonce_field( 'woocommerce-login' ); ?>
									<!-- LINK PARA REDEFINIR A SENHA -->
									<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
									<!-- INPUT PARA FALZER O LOGIN -->
									<input class="entrar" type="submit" class="" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />


								<?php do_action( 'woocommerce_login_form_end' ); ?>

							</form>

			      			<p>Ainda não possui uma cadastro</p>
			      			<!-- LINK PARA SE CADASTRAR -->
			      			<?php wp_nonce_field( 'woocommerce-register' ); ?>
			      			<?php echo '<a class="cadastrar" href="' .get_permalink(woocommerce_get_page_id('myaccount')). '?action=register">Cadastrar-se</a>'; ?>
			      			<?php do_action( 'woocommerce_register_form_end' ); ?>
			      		</div>
			      	</div>
			   	</div>
		    </div>
		  </div>
		</div>

	</header>
