<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package adegamalbec
 */

get_header(); ?>

		<?php 
	 	 if ( have_posts() ) : while( have_posts() ) : the_post();
				
			$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$foto = $foto[0];					
			 

	 	endwhile; endif; 
		?>

	<!-- POSTAGEM BLOG -->
	<div class="pg pg-postagem" style="display: ">
		<div class="container">
			
			<div class="row">
				<div class="col-md-3 sidebar-blog">
					<!-- SUB TÍTULO DA PÁGINA -->
					<div class="sub-titulo">
						<p class="borda-titulo">Categorias</p>
					</div>
						<?php


						// CATEGORIA ATUAL
						$categoriaAtual = get_the_category();
						$categoriaAtual = $categoriaAtual[0]->cat_name;
						// LISTA DE CATEGORIAS
						$arrayCategorias = array();
						$categorias=get_categories($args);
						foreach($categorias as $categoria) {
						$arrayCategorias[$categoria->cat_ID] = $categoria->name;
						$nomeCategoria = $arrayCategorias[$categoria->cat_ID];


					?>
					<a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a>
					<?php } ?>
				</div>
				<div class="col-md-9">
					
					

					<div class="post">
						<!-- TÍTULO DO POST -->
						<div class="titulo-post">
							<p>"<?php echo get_the_title() ?>"</p>
							
						</div>
						<!-- DATA -->
						<span class="data">
							<?php the_time('j \d\e F \d\e Y') ?>
							 - <?php the_time('g') ?> HORAS A TRÁS
						</span>
						<!-- IMAGEM -->
						<div class="bg-post" style="background: url(<?php echo $foto ?>) no-repeat;"></div>
						
						<!-- DESCRIÇÃO -->
						<p class="descricao"><?php echo get_the_content() ?></p>

						
					</div>	
					
					<div class="disqus">
						<?php
							//If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>				
					</div>
				</div>
				
			</div>
		
		</div>
	</div>

<?php

get_footer();
