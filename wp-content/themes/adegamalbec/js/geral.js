$(function(){

		/*******************************************************
        *  CARROSSEL DESTAQUE PÁGINA INICIAL
		*******************************************************/

		$(document).ready(function() {

	  		$("#carrossel-destaque").owlCarousel({

				items : 1,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
	  		});
	  		var carrossel_destaque = $("#carrossel-destaque").data('owlCarousel');
			$('.navegacaoDestqueFrent').click(function(){ carrossel_destaque.prev(); });
			$('.navegacaoDestqueTras').click(function(){ carrossel_destaque.next(); });

	 	});


		/*******************************************************
        *  CARROSSEL DESTAQUE DE PRODUTOS
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-produtos-destaque").owlCarousel({

				items : 4,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        mouseDrag: true,
		     //    autoplay:true,
			    // autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },
			        }
	  		});
	  		var carrossel_produtos_destaque = $("#carrossel-produtos-destaque").data('owlCarousel');
			$('.navegacaoDestque-produtoFrent').click(function(){ carrossel_produtos_destaque.prev(); });
			$('.navegacaoDestque-produtoTras').click(function(){ carrossel_produtos_destaque.next(); });

	 	});



		/*******************************************************
        *  CARROSSEL NOVIDADES PÁGINA INICIAL
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-produtos-novidades").owlCarousel({

				items : 4,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        // mouseDrag: true,
		        // autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },

			        }
	  		});
	  		var carrossel_produtos_novidades = $("#carrossel-produtos-novidades").data('owlCarousel');
			$('.navegacaoDestque-novidadesFrent').click(function(){ carrossel_produtos_novidades.prev(); });
			$('.navegacaoDestque-novidadesTras').click(function(){ carrossel_produtos_novidades.next(); });

	 	});


		/*******************************************************
        *  CARROSSEL NOVIDADES ADEGA INICIAL
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-produtos-vendidos").owlCarousel({

				items : 4,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        // mouseDrag: true,
		        // autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },

			        }
	  		});
	  		var carrossel_produtos_vendidos = $("#carrossel-produtos-vendidos").data('owlCarousel');
			$('.navegacaoDestque-vendidosFrent').click(function(){ carrossel_produtos_vendidos.prev(); });
			$('.navegacaoDestque-vendidosTras').click(function(){ carrossel_produtos_vendidos.next(); });

	 	});

	 	/*******************************************************
        *  CARROSSEL PAÍSES ADEGA INICIAL
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-paises").owlCarousel({

				items : 4,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        // mouseDrag: true,
		        // autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
		               	320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },

			        }
	  		});
	  		var carrossel_produtos_novidades_paises = $("#carrossel-paises").data('owlCarousel');
			$('.navegacaoDestque-novidades-paisesFrent').click(function(){ carrossel_produtos_novidades_paises.prev(); });
			$('.navegacaoDestque-novidades-paisesTras').click(function(){ carrossel_produtos_novidades_paises.next(); });

	 	});


	 	/*******************************************************
        *  CARROSSEL PAÍSES ADEGA INICIAL
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-produtos-novidade").owlCarousel({

				items : 4,
		        dots: true,
		        loop: true,
		        lazyLoad: true,
		        // mouseDrag: true,
		        // autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },

			        }
	  		});
	  		var carrossel_produtos_novidade = $("#carrossel-produtos-novidade").data('owlCarousel');
			$('.carrossel-produtos-novidadeFrente').click(function(){ carrossel_produtos_novidade.prev(); });
			$('.carrossel-produtos-novidadeTras').click(function(){ carrossel_produtos_novidade.next(); });

	 	});


	 	/*******************************************************
        *  CARROSSEL DESTAQUE SINGLE DE PRODUTOS
		*******************************************************/

	 	$(document).ready(function() {

	  		$("#carrossel-produtosSingle-novidades").owlCarousel({

				items : 4,
		        dots: true,
		        //loop: true,
		        lazyLoad: true,
		        // mouseDrag: true,
		        // autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 4000,
			    responsiveClass:false,
			        responsive:{
			            320:{
			                items:1
			            },
			            425:{
			                items:2
			            },
			            768:{
			                items:2
			            },
			            1024:{
			                items:4
			            },

			        }
	  		});
	  		var carrossel_produtosSingle_novidade = $("#carrossel-produtosSingle-novidades").data('owlCarousel');
			$('.navegacaoSingle-novidadesFrent').click(function(){ carrossel_produtosSingle_novidade.prev(); });
			$('.navegacaoSingle-novidadesTras').click(function(){ carrossel_produtosSingle_novidade.next(); });

	 	});

 		/*******************************************************
        *  SCRIPT PÁGINA DE PRODUTO TROCA DE FOTOS
		*******************************************************/
		$(document).ready(function() {

				$('.fotos').click(function(e){
				var urlImg = $(this).attr('data-url');

				$('#foto-principal').attr('src',urlImg);




				});
			});
		/*******************************************************
        *  SCRIPT PÁGINA DA LOJA  OPÇÕES DE LISTA E GRADE
		*******************************************************/
		$(document).ready(function() {

			$('#lista').click(function(e){
				$('#lista').css({"color":"#858585"})
				$('#grade').css({"color":"#CCCCCC"})
				$('.lista-grade').css({"display":"block","max-width":"inherit","min-height":"320px","text-align": "left"});
				$('.foto-produto').css({"display":"inline-block"});
				$('.conteudo').css({"display":"inline-block","vertical-align": "top", "width": "712px","padding":"0 0 0 50px"});
				$('.adicionar-aocarrinho').css({"left": "23%"});
				$('.conteudo .descricao-produto h2').css({"text-align":"left", "font-size": "15px"});
				$('.item .estrela').css({"margin":"0"});
				$('.item .preco').css({"text-align":"left"});
				$('.item .promocao').css({"text-align":"left"});
				$('.item .desconto').css({"text-align":"left"});
				$('.descricao-completa').css({"display":"inline-block"});

			});

			$('#grade').click(function(e){
				$('#grade').css({"color":"#858585"})
				$('#lista').css({"color":"#CCCCCC"})
				$('.lista-grade').css({"display":"inline-block","max-width":"234px","min-height":"468px","text-align": "initial"});
				$('.foto-produto').css({"display":"block"});
				$('.conteudo').css({"display":"block","vertical-align": "initial","width": "234px","padding":"initial","text-align":"center"});
				$('.adicionar-aocarrinho').css({"left": "50%"});
				$('.conteudo .descricao-produto h2').css({"text-align":"center","font-size": "13px"});
				$('.item .estrela').css({"margin":"0 auto"});
				$('.item .preco').css({"text-align":"center"});
				$('.item .promocao').css({"text-align":"center"});
				$('.item .desconto').css({"text-align":"center"});
				$('.descricao-completa').css({"display":"none"});

			}) ;

		});
		$(document).resize(function() {
			var  largura_tela =  $(window).width();

			if (largura_tela < 768) {
				$('.lista-grade').css({"display":"inline-block","max-width":"234px","min-height":"468px","text-align": "initial"});
				$('.foto-produto').css({"display":"block"});
				$('.conteudo').css({"display":"block","vertical-align": "initial","width": "234px","padding":"initial","text-align":"center"});
				$('.adicionar-aocarrinho').css({"left": "50%"});
				$('.conteudo .descricao-produto h2').css({"text-align":"center","font-size": "13px"});
				$('.item .estrela').css({"margin":"0 auto"});
				$('.item .preco').css({"text-align":"center"});
				$('.item .promocao').css({"text-align":"center"});
				$('.item .desconto').css({"text-align":"center"});
				$('.descricao-completa').css({"display":"none"});
			}


		});


		$( "#fisica" ).click(function(e) {
			$("#fisica").css({'background':'#f5f5f5'});
			$("#cpf").css({'display':'block'});
			$("#cnpj").css({'display':'none'});
			$("#razao-social").css({'display':'none'});
			$("#iEstadual").css({'display':'none'});
			$("#juridica").css({'background':'#fff'});
			$("#nomeFantasia").css({'display':'none'});
		});

		$( "#juridica" ).click(function(e) {
			$("#fisica").css({'background':'#fff'});
			$("#juridica").css({'background':'#f5f5f5'});
			$("#cpf").css({'display':'none'});
			$("#cnpj").css({'display':'block'});
			$("#razao-social").css({'display':'block'});
			$("#iEstadual").css({'display':'block'});
			$("#nomeFantasia").css({'display':'block'});
		});

		//$(".show-ywsl-box").text("com o Facebook");
		$(".show-ywsl-box").parent().html('Entre <a href="#" class="show-ywsl-box">com o Facebook</a>');

		$(document).ready(function() {

			$( "#colapse-silgle-1" ).click(function(e) {
			var condicao = $( "#colapse-silgle-1" ).attr('aria-expanded');
				if (condicao != "false") {

					$("#icon1").removeClass().addClass("fa fa-minus")
				}else {

					$("#icon1").removeClass().addClass("fa fa-plus")
				}
			});
		});
		$(document).ready(function() {

			$( "#colapse-silgle-2" ).click(function(e) {
			var condicao = $( "#colapse-silgle-2" ).attr('aria-expanded');
				if (condicao != "false") {

					$("#icon2").removeClass().addClass("fa fa-minus")
				}else {

					$("#icon2").removeClass().addClass("fa fa-plus")
				}
			});
		});

		// HOVER MENU
		$('li.dropdown').mouseover(function(e){
			$(this).addClass('open');
			$(this).find('a.dropdown-toggle').css('color','#292929');
		});
		$('li.dropdown').mouseout(function(e){
			$(this).find('a.dropdown-toggle').css('color','#fff');
		});
		$('li.dropdown ul').mouseout(function(){
			$(this).parent().removeClass('open');
		});
		$('li.dropdown').mouseout(function(){
			$(this).removeClass('open');
		});

		// SCRIPT PÁGINA SINGLE DE PRODUTO BOTÕES UNIDADE E ADICIONAR 6 UNIDADES
		$( "#botao-unidade" ).click(function(e) {
			var quantidadeVariacao = $('#inputUnidade').val();
			$(".quantity input").val(quantidadeVariacao);
			var variationId = $(this).attr("data-id");
			$(".variation_id").val(variationId);
			$('td.value #quantidade').focus();
			$('td.value #quantidade').val('Unidade').trigger('change');
			$(".single_add_to_cart_button").prop("disabled", false);
			$(".single_add_to_cart_button").click();
		});
		$( "#botao-caixa" ).click(function(e) {
			var quantidadeVariacaocaixa = $('#inputCaixa').val();
			$(".quantity input").val(quantidadeVariacaocaixa);
			var variationcixaId = $(this).attr("data-id");
			$(".variation_id").val(variationcixaId);
			$('td.value #quantidade').focus();
			$('td.value #quantidade').val('Caixa com 6 unidades').trigger('change');
			$(".single_add_to_cart_button").prop("disabled", false);
			$(".single_add_to_cart_button").click();
		});

		$(document).ready(function() {
			var largura = $(window).width();
			if (largura <= 425) {
				$( "#sidebar-pg" ).removeClass('in');
			}
		});

		$(document).ready(function() {
			$('.buy').click(function(e) {
				e.preventDefault();
				$(".loader").show();
				var unId = $(this).attr('data-unId');
				//alert(prodId);
				addToCart(unId);
				return false;
			});
			$('.buy6').click(function(e) {
				e.preventDefault();
				$(".loader").show();
				var sixId = $(this).attr('data-sixId');
				//alert(prodId);
				addToCart(sixId);
				return false;
			});
			function addToCart(p_id) {
				// $.get('/loja/?post_type=product&add-to-cart=' + p_id +'&quantity=1', function() {
				// 	$(".loader").hide();
				// 	$("#modalCartSuccess").show();
				// 	var qtdCart = $("#qtd-header").text();
				// 	qtdCart = Number(qtdCart);
				// 	qtdCart= qtdCart + 1;
				// 	$("#qtd-header").text(qtdCart);
			 //  	});
			 //
			 jQuery.ajax({
			 	type: 'POST',
			 	url: '/?post_type=product&add-to-cart='+p_id,
			 	data: { 'product_id':  p_id,
			 	'quantity': '1'},
			 	success: function(response, textStatus, jqXHR){

                var new_product_html = "<li"+ response.product_id +"</li>";
                console.log(response);
                $(".informacoes-carrinho").append(new_product_html);
            	$(".loader").hide();
				$("#modalCartSuccess").show();
				var qtdCart = $("#qtd-header").text();
				qtdCart = Number(qtdCart);
				qtdCart= qtdCart + 1;
				$("#qtd-header").text(qtdCart);

                console.log("Product added");

            },
            // dataType: 'JSON'
        });
			}

			$("#modalCartSuccessFechar").click(function(){
				$("#modalCartSuccess").hide();
			});
		});
		$(document).ready(function() {
			$('<smaall class="info-cadastromensagem">Necessário para envio de nota fiscal.</smaall>').insertAfter($( "#billing_cnpj" ));
			$('<smaall class="info-cadastromensagem">Necessário para envio de nota fiscal.</smaall>').insertAfter($( "#billing_cpf" ));
				$(".info-cadastromensagem").css({
					"display":"block",
					"width":"100%",
					"max-width":"457px",
					"margin":"auto",
			});
			$('#nomeFantasia').insertAfter($( "#razao-social" ));
		});

		$('#menu-item-711').append($('#megamenuVinhos'));
	});
