<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	global $product;
	global $wp;
	$current_url = home_url(add_query_arg(array(),$wp->request));
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

	<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php


			if ($product->product_type == "variable") {
			$available_variations = $product->get_available_variations();
			//UNIDADE
			 $nomeAtributoUnidade = $available_variations[0]["attribute_pa_quantidade"];
			 $precounidade = $available_variations[0]["display_regular_price"];
			 $precounidadehtml = $available_variations[0]["price_html"];
			 $precounidadeRegular = $available_variations[0]["display_regular_price"];
			 $idunidade = $available_variations[0]["variation_id"];
			 $quantidadeunidade = $available_variations[0]["availability_html"];
			//CAIXA
			 $nomeAtributoCaixa = $available_variations[1]["attribute_pa_quantidade"];
			 $precocaixa = $available_variations[1]["display_regular_price"];
			 $precocaixahtml = $available_variations[1]["price_html"];
			 $idcaixa = $available_variations[1]["variation_id"];
			 $quantidadecaixa = $available_variations[1]["availability_html"];

			}

		    $numleft  = $product->get_stock_quantity();
		    if($numleft==0):
		       // fora de estoque
		?>
			<!-- DETALHES DO PRDUTO -->
			<div class="produto-loja">

				<!-- FOTO PRODUTO -->
				<div class="row">
					<div class="col-md-5">

						<div class="foto-detalheproduto">
							<?php
								$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
								$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
									'title'	=> get_the_title( get_post_thumbnail_id() )
									) );
							?>
							<!-- FOTO DO PRODUTO -->
							<a href="<?php echo $image_link; ?>" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img id="foto-principal" src="<?php echo $image_link ?>" class="img-resposive attachment-shop_single size-shop_single wp-post-image" alt="v4" title="v4"></a>

							<!-- DESCONTO PORCENTAGEM -->
							<?php

								$product->get_price_html();
								$precoatual = $product->price;
								// $precoantigo = $product->regular_price;
								$valor = explode(";", $product->get_price_html());
								$precoantigo = $valor[2];


								$desconto = 0;
								if ($precoantigo != 0)
								$desconto = (($precoatual * 100) / $precoantigo) - 100;

							if ($product->is_on_sale() == "true") {
								# code...

							?>


							<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
							<?php }; ?>

						</div>

						<!-- OUTRAS FOTOS  -->
						<div class="galeria">
						<?php
						$attachment_ids = $product->get_gallery_attachment_ids();

						if ( $attachment_ids ) {
							$loop 		= 0;
							$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
							?>
							<div class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

								foreach ( $attachment_ids as $attachment_id ) {

									$classes = array( 'zoom' );

									if ( $loop === 0 || $loop % $columns === 0 )
										$classes[] = 'first';

									if ( ( $loop + 1 ) % $columns === 0 )
										$classes[] = 'last';

									$image_link = wp_get_attachment_url( $attachment_id );

									if ( ! $image_link )
										continue;

									$image_title 	= esc_attr( get_the_title( $attachment_id ) );
									$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

									$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title
										) );

									$image_class = esc_attr( implode( ' ', $classes ) );

									echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

									$loop++;
								}

							?></div>
							<?php
						}?>

						</div>

					</div>

					<!-- DESCRIÇÃO DO PRODUTO -->
					<div class="col-md-7">
						<div class="descricao-produto">
							<!-- ID -->
							<small class="id">ID <?php echo $product->get_sku(); ?></small>
							<!-- NOME -->
							<h2><?php the_title(); ?></h2>
							<!-- AVALIAÇÕES -->
							<div class="avaliacoes">
								<div class="estrela">
									<?php
										if ($product->get_average_rating() == null || $product->get_average_rating() == '0') {
											echo '<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>';
										}else{
											woocommerce_template_single_rating();
										}

									 ?>





								</div>
								<?php
							        $numleft  = $product->get_stock_quantity();
							        if($numleft==0) {
							           // fora de estoque
							            echo '<div class="zero-estoque"><i class="fa fa-times"></i> Indisponível</div>';
							        }
							        else {
							            echo '<div class="tem-estoque"><i class="fa fa-check"></i> Em Estoque</div>';
					        		}
								?>

							</div>
							<!-- DESCRIÇÃO -->
							<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>


							<div class="produtosgotado">
								<p>Produto esgotado. Avise-me quando chegar</p>
								<span>Enviaremos um e-mail informando quando o produto estiver no estoque</span>
								<?php
									// AVISE-ME QUANDO CHEGAR
									if($product->stock <= 0){
									do_action('campos_avise_me',$product->id);
									}
								 ?>

							</div>

						</div>
					</div>
				</div>

				<!-- REDES SOCIAIS -->
				<div class="redessociais">
					<p><a href="">Compartilhe</a></p>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url; ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-facebook"></i>
					</a>
					<a href="http://twitter.com/share?text=Confira%20esta%20notícia:&hashtags=post_compartilhado,artigo_interessante" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-twitter"></i>
					</a>
					<a href="http://pinterest.com/pin/create/button/?url=<?php echo $current_url; ?>&media=<?php echo $image_link; ?>&description=<?php echo $get_the_content; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					    <i class="fa fa-pinterest-p"></i>
					</a>
					<a href="https://plus.google.com/share?url=<?php echo $current_url; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-google-plus"></i>
					</a>
				</div>

				<!-- DETALHES DO PRODUTO -->
				<div class="detalhe-produto">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<span><i class="fa fa-minus" aria-hidden="true"></i></span>
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Descrição do produto
								</a>
							</div>

							<div id="collapseOne" class="panel-collapse collapse in row" role="tabpanel" aria-labelledby="headingOne">

								<div class="col-sm-12">
									<div class="table-responsive panel-body">


										<p style="margin-bottom: 20px;"><?php echo get_the_content()	?></p>

										<?php
										$has_row    = false;
										$alt        = 1;
										$attributes = $product->get_attributes();

										ob_start();

										?>
										<table class="shop_attributes table">
											<?php foreach ( $attributes as $attribute ) :
											if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
												continue;
											} else {
												$has_row = true;
											}
											?>
											<?php if($attribute['name'] != "pa_quantidade"): ?>
											<tr class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
												<th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
												<td><?php
													if ( $attribute['is_taxonomy'] ) {

														$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
														echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

													} else {

														$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
														echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

													}
													?></td>
												</tr>
											<?php endif; ?>
											<?php endforeach; ?>

										</table>
										<?php
										if ( $has_row ) {
											echo ob_get_clean();
										} else {
											ob_end_clean();
										}
										?>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<?php
					/**
					 * woocommerce_after_single_product_summary hook.
					 *
					 * @hooked woocommerce_output_product_data_tabs - 10
					 * @hooked woocommerce_upsell_display - 15
					 * @hooked woocommerce_output_related_products - 20
					 */
					do_action( 'woocommerce_after_single_product_summary' );
				?>

				<!-- COMENTÁRIOS -->
				<div class="detalhe-produto" id="reviews">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<span><i class="fa fa-minus" aria-hidden="true"></i></span>
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
									Comentários
								</a>
							</div>

							<div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="copia-comentarios"></div>

									<button data-toggle="collapse" data-target="#demo4">Escreva um comentário <i class="fa fa-plus" aria-hidden="true"></i></button>
									<div id="demo4" class="collapse">
										<div class="copia-avaliar">

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>



				<!-- PRODUTOS NOVOS -->
				<div class="produto-destaque">

					<span class="subtitulo">Produtos relacionados</span>

					<!-- BOTÕES DE NAVEGAÇÃO -->
					<div class="botoes-produtos">
						<button class="navegacaoSingle-novidadesFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
						<button class="navegacaoSingle-novidadesTras hidden-xs"><i class="fa fa-angle-right"></i></button>
					</div>

					<!-- PRODUTOS EM DESTAQUE -->
					<!-- PRODUTOS EM DESTAQUE -->
					<div class="carrossel-produtos-destaque" >
						<div id="carrossel-produtosSingle-novidades" class="owl-Carousel">
							<!-- PRODUTO -->
							<?php
								$related = $product->get_related();
								$relatedQtd = count($related);
								$upsells = $product->get_upsells();
								$upsellsQtd = count($upsells);
								$meta_query = WC()->query->get_meta_query();
								$args = array(
									'post_type'           => 'product',
									'ignore_sticky_posts' => 1,
									'no_found_rows'       => 1,
									'posts_per_page'      => $upsellsQtd,
									'orderby'             => 'rand',
									'post__in'            => $upsells,
									'post__not_in'        => array( $product->id ),
									'meta_query'          => $meta_query
								);


					            $loop = new WP_Query( $args );
					            while ( $loop->have_posts() ) : $loop->the_post(); global $product;

							?>
							<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">
												<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

											<?php
											if($product->product_type == "variable"){
												$prodVars = $product->get_available_variations();
												foreach ($prodVars as $prodVar) {

													if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
														$unId = $prodVar['variation_id'];
														echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><a class="buy areaImgComprar link-botao-comprar" href="#" data-unid="682"><img src="http://adegamalbec.pixd.com.br/wp-content/themes/adegamalbec/img/sacola.png"><span>Adicionar ao carrinho</span></a></a>';
													}
												}
											}else{
												$unId = $product->id;
												if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
													echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><a class="buy areaImgComprar link-botao-comprar" href="#" data-unid="682"><img src="http://adegamalbec.pixd.com.br/wp-content/themes/adegamalbec/img/sacola.png"><span>Adicionar ao carrinho</span></a></a>';
												}
											}

											?>


										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h3><?php the_title(); ?></h3>
									</div>

									<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo '
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>
									<!-- PREÇO -->
									<?php if ($product->price != ''): ?>
										<p class="preco">R$ <?php echo number_format($product->price,2,",","." ); ?></p>
									<?php endif ?>

									<?php
										$price   = $product->get_price();
										$price3x = $price / 3;
										$price2x = $price / 2;
									?>

									<?php if ($price >= 120): ?>
									<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
									<?php elseif($price >= 80): ?>
									<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
									<?php else: ?>
									<p class="promocao" style="height: 20px;"></p>
									<?php endif; ?>

									<?php
									if($product->product_type == "variable"){
										$prodVars = $product->get_available_variations();
										foreach ($prodVars as $prodVar) {
											if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
												$sixId = $prodVar['variation_id'];
												$str = $prodVar["attributes"]["attribute_pa_quantidade"];
												$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
												$qTD = str_replace('-', '', $qTD);


												$sixDiv = $prodVar['display_price'] / $qTD;

												$sixDiv = money_format('%.2n', $sixDiv);

												echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>6un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

											} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
												echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
											}
										}
									}else{
										$quantidadeProduto = $product->get_stock_quantity();
										if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
											echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
										}
									}

									?>

								</div>
							<?php endwhile;wp_reset_query(); ?>

						</div>
					</div>
				</div>

			</div>

		<?php else: ?>
		    <!-- DETALHES DO PRDUTO  SEM VARIAÇÃO-->
			<div class="produto-loja">

				<!-- FOTO PRODUTO -->
				<div class="row">
					<div class="col-md-5">

						<div class="foto-detalheproduto">
							<?php
								$image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
								$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
									'title'	=> get_the_title( get_post_thumbnail_id() )
									) );
							?>
							<!-- FOTO DO PRODUTO -->
							<a href="<?php echo $image_link; ?>" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]"><img id="foto-principal" src="<?php echo $image_link ?>" class="img-resposive attachment-shop_single size-shop_single wp-post-image" alt="v4" title="v4"></a>

							<!-- DESCONTO PORCENTAGEM -->
							<?php

								$product->get_price_html();
								$precoatual = $product->price;
								// $precoantigo = $product->regular_price;
								$valor = explode(";", $product->get_price_html());
								$precoantigo = $valor[2];


								$desconto = 0;
								if ($precoantigo != 0)
								$desconto = (($precoatual * 100) / $precoantigo) - 100;

							if ($product->is_on_sale() == "true") {
								# code...

							?>


							<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
							<?php }; ?>

						</div>

						<!-- OUTRAS FOTOS  -->

						<div class="galeria">
						<?php
						$attachment_ids = $product->get_gallery_attachment_ids();

						if ( $attachment_ids ) {
							$loop 		= 0;
							$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
							?>
							<div class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

								foreach ( $attachment_ids as $attachment_id ) {

									$classes = array( 'zoom' );

									if ( $loop === 0 || $loop % $columns === 0 )
										$classes[] = 'first';

									if ( ( $loop + 1 ) % $columns === 0 )
										$classes[] = 'last';

									$image_link = wp_get_attachment_url( $attachment_id );

									if ( ! $image_link )
										continue;

									$image_title 	= esc_attr( get_the_title( $attachment_id ) );
									$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

									$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
										'title'	=> $image_title,
										'alt'	=> $image_title
										) );

									$image_class = esc_attr( implode( ' ', $classes ) );

									echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

									$loop++;
								}

							?></div>
							<?php
						}?>

						</div>

					</div>

					<!-- DESCRIÇÃO DO PRODUTO -->
					<div class="col-md-7">
						<div class="descricao-produto">
							<!-- ID -->
							<small class="id">ID <?php echo $product->get_sku(); ?></small>
							<!-- NOME -->
							<h2><?php the_title(); ?></h2>
							<!-- AVALIAÇÕES -->
							<div class="avaliacoes">
								<div class="estrela">
								<?php
										if ($product->get_average_rating() == null || $product->get_average_rating() == '0') {
											echo '<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>';
										}else{
											woocommerce_template_single_rating();
										}

									 ?>
								</div>
								<?php
							        $numleft  = $product->get_stock_quantity();
							        if($numleft==0) {
							           // fora de estoque
							            echo '<div class="zero-estoque"><i class="fa fa-times"></i> Indisponível</div>';
							        }
							        else {
							            echo '<div class="tem-estoque"><i class="fa fa-check"></i> Em Estoque</div>';
					        		}
								?>

							</div>
							<!-- DESCRIÇÃO -->
							<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
								<?php

							if ($idcaixa == null) {




							?>

							<div class="row info-qtd-preco">

								<!-- UNIDADE -->
								<div class="col-md-3">

									<p class="attrQuantidade">Unidade</p>
								</div>

								<!-- PREÇOS -->
								<div class="col-md-3 text-left">
									<div class="valor text-left">
										<span class="preco"><!-- <?php echo $precounidadeRegular  ?> --> <?php echo $product->get_price_html(); ?><!-- <?php echo $quantidadeunidade ?> --></span>
									</div>
								</div>

								<div class="col-md-6">
									<div class="comprar text-right">

										<div class="quantity">
											<input type="number" step="1" min="1" max="" name="quantityUnidade" id="inputUnidade" value="1" title="Qtd" class="input-text qty text quantidade" size="4">
										</div>

										<button type="submit" data-id="<?php echo $idunidade ?>"class="botao-unidade" id="botao-unidade">Comprar <img src="<?php bloginfo('template_directory'); ?>/img/icon-botao.png" alt=""><span><div class="txt">Comprar 6 unidades </div> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 473.8 473.8" style="enable-background:new 0 0 473.8 473.8;" xml:space="preserve" width="28px" height="28px"><g>	<path d="M454.8,111.7c0-1.8-0.4-3.6-1.2-5.3c-1.6-3.4-4.7-5.7-8.1-6.4L241.8,1.2c-3.3-1.6-7.2-1.6-10.5,0L25.6,100.9   c-4,1.9-6.6,5.9-6.8,10.4v0.1c0,0.1,0,0.2,0,0.4V362c0,4.6,2.6,8.8,6.8,10.8l205.7,99.7c0.1,0,0.1,0,0.2,0.1   c0.3,0.1,0.6,0.2,0.9,0.4c0.1,0,0.2,0.1,0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.3c0.1,0,0.2,0.1,0.3,0.1c0.3,0.1,0.7,0.1,1,0.2   c0.1,0,0.2,0,0.3,0c0.4,0,0.9,0.1,1.3,0.1c0.4,0,0.9,0,1.3-0.1c0.1,0,0.2,0,0.3,0c0.3,0,0.7-0.1,1-0.2c0.1,0,0.2-0.1,0.3-0.1   c0.3-0.1,0.6-0.2,0.9-0.3c0.1,0,0.2-0.1,0.4-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.1,0,0.1,0,0.2-0.1l206.3-100c4.1-2,6.8-6.2,6.8-10.8   V112C454.8,111.9,454.8,111.8,454.8,111.7z M236.5,25.3l178.4,86.5l-65.7,31.9L170.8,57.2L236.5,25.3z M236.5,198.3L58.1,111.8   l85.2-41.3L321.7,157L236.5,198.3z M42.8,131.1l181.7,88.1v223.3L42.8,354.4V131.1z M248.5,442.5V219.2l85.3-41.4v58.4   c0,6.6,5.4,12,12,12s12-5.4,12-12v-70.1l73-35.4V354L248.5,442.5z" fill="#FFFFFF"/></g></svg></span> </button>

									</div>
								</div>

							</div>

							<hr>
							<div class="comprar text-right">
								<div class="calculador">
									<!-- CALCULAR FRETE -->
									<a data-toggle="collapse" data-target="#demo">Calcular Frete</a>


									<div id="demo" class="collapse">
									<?php
										add_action( 'teste', array( 'WC_Correios_Product_Shipping_Simulator', 'simulator' ), 25 );
										do_action('teste');
									?>
									</div>

								</div>
								<div class="formulario">
									<?php

										/**
										 * woocommerce_after_shop_loop_item hook
										 *
										 * @hooked woocommerce_template_loop_add_to_cart - 10
										 */
										//do_action( 'woocommerce_after_shop_loop_item' );

										// if($qtdEstoque > 0) {
											woocommerce_template_single_add_to_cart();
										// }
									?>
								</div>

							</div>
							<?php }else{ ?>

								<div class="row info-qtd-preco">

									<!-- UNIDADE -->
									<div class="col-md-3">

										<p class="attrQuantidade">Unidade</p>
									</div>

									<!-- PREÇOS -->
									<div class="col-md-3 text-left">
										<div class="valor text-left">
											<span class="preco"><?php echo $precounidadehtml  ?><!-- <?php echo $product->get_price_html(); ?> --><!-- <?php echo $quantidadeunidade ?> --></span>
										</div>
									</div>

									<div class="col-md-6">
										<div class="comprar text-right">

											<div class="quantity">
												<input type="number" step="1" min="1" max="" name="quantityUnidade" id="inputUnidade" value="1" title="Qtd" class="input-text qty text quantidade" size="4">
											</div>

											<button type="submit" data-id="<?php echo $idunidade ?>"class="botao-unidade" id="botao-unidade">Comprar <img src="<?php bloginfo('template_directory'); ?>/img/icon-botao.png" alt=""><span><div class="txt">Comprar 6 unidades </div> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 473.8 473.8" style="enable-background:new 0 0 473.8 473.8;" xml:space="preserve" width="28px" height="28px"><g>	<path d="M454.8,111.7c0-1.8-0.4-3.6-1.2-5.3c-1.6-3.4-4.7-5.7-8.1-6.4L241.8,1.2c-3.3-1.6-7.2-1.6-10.5,0L25.6,100.9   c-4,1.9-6.6,5.9-6.8,10.4v0.1c0,0.1,0,0.2,0,0.4V362c0,4.6,2.6,8.8,6.8,10.8l205.7,99.7c0.1,0,0.1,0,0.2,0.1   c0.3,0.1,0.6,0.2,0.9,0.4c0.1,0,0.2,0.1,0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.3c0.1,0,0.2,0.1,0.3,0.1c0.3,0.1,0.7,0.1,1,0.2   c0.1,0,0.2,0,0.3,0c0.4,0,0.9,0.1,1.3,0.1c0.4,0,0.9,0,1.3-0.1c0.1,0,0.2,0,0.3,0c0.3,0,0.7-0.1,1-0.2c0.1,0,0.2-0.1,0.3-0.1   c0.3-0.1,0.6-0.2,0.9-0.3c0.1,0,0.2-0.1,0.4-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.1,0,0.1,0,0.2-0.1l206.3-100c4.1-2,6.8-6.2,6.8-10.8   V112C454.8,111.9,454.8,111.8,454.8,111.7z M236.5,25.3l178.4,86.5l-65.7,31.9L170.8,57.2L236.5,25.3z M236.5,198.3L58.1,111.8   l85.2-41.3L321.7,157L236.5,198.3z M42.8,131.1l181.7,88.1v223.3L42.8,354.4V131.1z M248.5,442.5V219.2l85.3-41.4v58.4   c0,6.6,5.4,12,12,12s12-5.4,12-12v-70.1l73-35.4V354L248.5,442.5z" fill="#FFFFFF"/></g></svg></span> </button>

										</div>
									</div>

								</div>
								<hr>


								<div class="row info-qtd-preco">
									<!-- UNIDADE -->
									<?php

										$result = woocommerce_get_product_terms($product->id, 'pa_quantidade', 'names');

										foreach ($result as $result) {
										 	$variacao = $result;

											if ($variacao != "Unidade") {
												$quantidadeCaixa = $variacao;
											}else{
												$unidade = $variacao;
											}
										 }


										$str = $quantidadeCaixa;
										$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
										$qTD = str_replace('-', '', $qTD);

										?>

									<div class="col-md-3">

										<p class="attrQuantidade">Caixa com <?php echo $qTD ?> unidades </p>
										<span>Total: <strong>R$<?php echo $precocaixa ?></strong></span>


									</div>
									<!-- PREÇOS -->
									<div class="col-md-3 text-left">
										<div class="valor text-left">
											<span class="preco">R$<?php $caixaPrice = $precocaixa / $qTD; echo number_format($caixaPrice,2,",","." ); ?><!-- <?php echo $quantidadecaixa ?> --></span>
											<span class="info-unidade">por unidade</span>
										</div>
									</div>
									<div class="col-md-6">
										<div class="comprar text-right">

											<div class="quantity">
												<input type="number" step="1" min="1" max="" name="quantityCaixa" id="inputCaixa" value="1" title="Qtd" class="input-text qty text quantidade" size="4">
											</div>

											<button data-id="<?php echo $idcaixa ?>" id="botao-caixa" type="submit" class="botao-unidade"><div class="txt">Comprar <?php echo $qTD ?>  <small>unidades</small>  </div><img src="<?php bloginfo('template_directory'); ?>/img/dado.png" alt=""><span><div class="txt">Comprar <?php echo $qTD ?>  unidades </div> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 473.8 473.8" style="enable-background:new 0 0 473.8 473.8;" xml:space="preserve" width="28px" height="28px"><g>	<path d="M454.8,111.7c0-1.8-0.4-3.6-1.2-5.3c-1.6-3.4-4.7-5.7-8.1-6.4L241.8,1.2c-3.3-1.6-7.2-1.6-10.5,0L25.6,100.9   c-4,1.9-6.6,5.9-6.8,10.4v0.1c0,0.1,0,0.2,0,0.4V362c0,4.6,2.6,8.8,6.8,10.8l205.7,99.7c0.1,0,0.1,0,0.2,0.1   c0.3,0.1,0.6,0.2,0.9,0.4c0.1,0,0.2,0.1,0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.3c0.1,0,0.2,0.1,0.3,0.1c0.3,0.1,0.7,0.1,1,0.2   c0.1,0,0.2,0,0.3,0c0.4,0,0.9,0.1,1.3,0.1c0.4,0,0.9,0,1.3-0.1c0.1,0,0.2,0,0.3,0c0.3,0,0.7-0.1,1-0.2c0.1,0,0.2-0.1,0.3-0.1   c0.3-0.1,0.6-0.2,0.9-0.3c0.1,0,0.2-0.1,0.4-0.1c0.3-0.1,0.6-0.2,0.9-0.4c0.1,0,0.1,0,0.2-0.1l206.3-100c4.1-2,6.8-6.2,6.8-10.8   V112C454.8,111.9,454.8,111.8,454.8,111.7z M236.5,25.3l178.4,86.5l-65.7,31.9L170.8,57.2L236.5,25.3z M236.5,198.3L58.1,111.8   l85.2-41.3L321.7,157L236.5,198.3z M42.8,131.1l181.7,88.1v223.3L42.8,354.4V131.1z M248.5,442.5V219.2l85.3-41.4v58.4   c0,6.6,5.4,12,12,12s12-5.4,12-12v-70.1l73-35.4V354L248.5,442.5z" fill="#FFFFFF"/></g></svg></span> </button>

										</div>
									</div>

								</div>

								<div class="comprar text-right">
									<div class="calculador">
										<!-- CALCULAR FRETE -->
										<a data-toggle="collapse" data-target="#demo">Calcular Frete</a>


										<div id="demo" class="collapse">
										<?php
											add_action( 'teste', array( 'WC_Correios_Product_Shipping_Simulator', 'simulator' ), 25 );
											do_action('teste');
										?>
									</div>

								</div>

									<div class="formulario">
										<?php

											/**
											 * woocommerce_after_shop_loop_item hook
											 *
											 * @hooked woocommerce_template_loop_add_to_cart - 10
											 */
											//do_action( 'woocommerce_after_shop_loop_item' );

											// if($qtdEstoque > 0) {
												woocommerce_template_single_add_to_cart();
											// }
										?>

									</div>
								</div>
							<?php }; ?>
							</div>

					</div>
				</div>

				<!-- REDES SOCIAIS -->
				<div class="redessociais">
					<p><a href="">Compartilhe</a></p>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url; ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-facebook"></i>
					</a>
					<a href="http://twitter.com/share?text=Confira%20esta%20notícia:&hashtags=post_compartilhado,artigo_interessante" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-twitter"></i>
					</a>
					<a href="http://pinterest.com/pin/create/button/?url=<?php echo $current_url; ?>&media=<?php echo $image_link; ?>&description=<?php echo $get_the_content; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					    <i class="fa fa-pinterest-p"></i>
					</a>
					<a href="https://plus.google.com/share?url=<?php echo $current_url; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fa fa-google-plus"></i>
					</a>
				</div>

				<!-- DETALHES DO PRODUTO -->
				<div class="detalhe-produto">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<span><i class="fa fa-plus" id="icon1" aria-hidden="true"></i></span>
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="colapse-silgle-1" aria-expanded="true" aria-controls="collapseOne">
									Descrição do produto
								</a>
							</div>

							<div id="collapseOne" class="panel-collapse collapse in row" role="tabpanel" aria-labelledby="headingOne">

								<div class="col-sm-12">
									<div class="table-responsive panel-body">

										<p style="margin-bottom: 20px;"><?php echo get_the_content()	?></p>

										<?php
										$has_row    = false;
										$alt        = 1;
										$attributes = $product->get_attributes();

										ob_start();

										?>
										<table class="shop_attributes table">
											<?php foreach ( $attributes as $attribute ) :
											if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
												continue;
											} else {
												$has_row = true;
											}
											?>
											<?php if($attribute['name'] != "pa_quantidade"): ?>
											<tr class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
												<th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
												<td><?php
													if ( $attribute['is_taxonomy'] ) {

														$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
														echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

													} else {

														$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
														echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

													}
													?></td>
												</tr>
											<?php endif; ?>
											<?php endforeach; ?>

										</table>
										<?php
										if ( $has_row ) {
											echo ob_get_clean();
										} else {
											ob_end_clean();
										}
										?>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<?php
					/**
					 * woocommerce_after_single_product_summary hook.
					 *
					 * @hooked woocommerce_output_product_data_tabs - 10
					 * @hooked woocommerce_upsell_display - 15
					 * @hooked woocommerce_output_related_products - 20
					 */
					//do_action( 'woocommerce_after_single_product_summary' );
				?>

				<!-- COMENTÁRIOS -->
				<div class="detalhe-produto" id="reviews">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<span><i class="fa fa-plus" id="icon2" aria-hidden="true"></i></span>
								<a role="button" data-toggle="collapse" id="colapse-silgle-2"  data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
									Comentários
								</a>
							</div>

							<div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="copia-comentarios"></div>

									<button data-toggle="collapse" data-target="#demo1">Escreva um comentário <i class="fa fa-plus" aria-hidden="true"></i></button>
									<div id="demo1" class="collapse">
										<div class="copia-avaliar">
											<?php
												//If comments are open or we have at least one comment, load up the comment template.
												if ( comments_open() || get_comments_number() ) :
													comments_template();
												endif;
											?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- PRODUTOS NOVOS -->
				<div class="produto-destaque">

					<span class="subtitulo">Produtos relacionados</span>

					<!-- BOTÕES DE NAVEGAÇÃO -->
					<div class="botoes-produtos">
						<button class="navegacaoSingle-novidadesFrent hidden-xs"><i class="fa fa-angle-left"></i></button>
						<button class="navegacaoSingle-novidadesTras hidden-xs"><i class="fa fa-angle-right"></i></button>
					</div>

					<!-- PRODUTOS EM DESTAQUE -->
					<div class="carrossel-produtos-destaque" >
						<div id="carrossel-produtosSingle-novidades" class="owl-Carousel">
							<!-- PRODUTO -->
							<?php
								$related = $product->get_related();
								$relatedQtd = count($related);
								$upsells = $product->get_upsells();
								$upsellsQtd = count($upsells);
								$meta_query = WC()->query->get_meta_query();
								$args = array(
									'post_type'           => 'product',
									'ignore_sticky_posts' => 1,
									'no_found_rows'       => 1,
									'posts_per_page'      => $upsellsQtd,
									'orderby'             => 'rand',
									'post__in'            => $upsells,
									'post__not_in'        => array( $product->id ),
									'meta_query'          => $meta_query
								);


					            $loop = new WP_Query( $args );
					            while ( $loop->have_posts() ) : $loop->the_post(); global $product;

							?>
							<div class="item">
									<!-- FOTO -->
									<a href="<?php the_permalink(); ?>">
										<div class="foto-produto">
												<?php

											$product->get_price_html();
											$precoatual = $product->price;
											// $precoantigo = $product->regular_price;
											$valor = explode(";", $product->get_price_html());
											$precoantigo = $valor[2];


											$desconto = 0;
											if ($precoantigo != 0)
											$desconto = (($precoatual * 100) / $precoantigo) - 100;

										if ($product->is_on_sale() == "true") {
											# code...

										?>


										<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
										<?php }; ?>
											<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" class="img-responsive" alt="Placeholder" />'; ?>

											<?php
											if($product->product_type == "variable"){
												$prodVars = $product->get_available_variations();
												foreach ($prodVars as $prodVar) {

													if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
														$unId = $prodVar['variation_id'];
														echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><a class="buy areaImgComprar link-botao-comprar" href="#" data-unid="682"><img src="http://adegamalbec.pixd.com.br/wp-content/themes/adegamalbec/img/sacola.png"><span>Adicionar ao carrinho</span></a></a>';
													}
												}
											}else{
												$unId = $product->id;
												if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
													echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><a class="buy areaImgComprar link-botao-comprar" href="#" data-unid="682"><img src="http://adegamalbec.pixd.com.br/wp-content/themes/adegamalbec/img/sacola.png"><span>Adicionar ao carrinho</span></a></a>';
												}
											}

											?>


										</div>
									</a>
									<!-- DESCRIÇÃO -->
									<div class="descricao-produto">
										<h3><?php the_title(); ?></h3>
									</div>

									<div class="estrela">
										<?php
											 $average = $product->get_average_rating();
											 $avaliacao = explode(".", $average);


											 if ($avaliacao[0] == "5") {

											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>

										 			';
											 }elseif ($avaliacao[0] == "4") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "3") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "2") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>


											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
										 			';
											 }elseif ($avaliacao[0] == "1") {
											 	echo '
											 			<i class="fa fa-star"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>
											 			<i class="fa fa-star-o"></i>

											 			<i class="fa fa-star-o"></i>

										 			';
											 }elseif ($avaliacao[0] == "0") {
											 	echo '
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>
											 		<i class="fa fa-star-o"></i>

										 			';
											 }

										?>

									</div>
									<!-- PREÇO -->
									<?php if ($product->price != ''): ?>
										<p class="preco">R$ <?php echo number_format($product->price,2,",","." ); ?></p>
									<?php endif ?>
									<?php
										$price   = $product->get_price();
										$price3x = $price / 3;
										$price2x = $price / 2;
									?>

									<?php if ($price >= 120): ?>
									<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
									<?php elseif($price >= 80): ?>
									<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
									<?php else: ?>
									<p class="promocao" style="height: 20px;"></p>
									<?php endif; ?>

									<?php
									if($product->product_type == "variable"){
										$prodVars = $product->get_available_variations();
										foreach ($prodVars as $prodVar) {
											if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
												$sixId = $prodVar['variation_id'];
												$str = $prodVar["attributes"]["attribute_pa_quantidade"];
												$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
												$qTD = str_replace('-', '', $qTD);

												$sixDiv = $prodVar['display_price'] / $qTD;

												$sixDiv = money_format('%.2n', $sixDiv);

												echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>6un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

											} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
												echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
											}
										}
									}else{
										$quantidadeProduto = $product->get_stock_quantity();
										if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
											echo '<p class="esgotado"><i class="fa fa-times"></i> Esgotado</p>';
										}
									}

									?>

								</div>
							<?php endwhile;wp_reset_query(); ?>

						</div>
					</div>
				</div>

			</div>

		<?php endif; ?>
	</div>
<script>

		var div = document.getElementsByClassName("commentlist");
		$(".copia-comentarios").append(div);
		var div2 = document.getElementById("review_form_wrapper");
		$(".copia-avaliar").append(div2);
		$(".woocommerce-tabs").remove();
		$(".related.products").remove();



</script>

<?php do_action( 'woocommerce_after_single_product' ); ?>
