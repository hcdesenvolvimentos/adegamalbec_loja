<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $configuracao;
get_header( 'shop' ); ?>
	<!-- PÁGINA LOJA  -->
	<div class="pg pg-loja">
		<div class="container">
			<div class="row">
				<!-- SIDEBAR -->
				<div class="col-md-2">
					<!-- LOJA -->
					<?php include (TEMPLATEPATH . '/inc/menu-lateral.php'); ?>
				
				</div>

				<!-- CONTEÚDO LOJA  -->
				<div class="col-md-10">
					<div class="conteudo-loja">
						
						<?php global $wp_query;
							$cat = $wp_query->get_queried_object();
							$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
							$image = wp_get_attachment_url( $thumbnail_id );
							if ( $image ) {
							  echo '<div class="bg-loja" style="background:url('.$image.')"></div>';
						} 
							$decricao = explode(";", $cat->description);
							
						?>				
						
						
						<!-- DESCRIÇÃO  -->
						<p class="descricao-loja page-description"><?php echo $decricao[1]  ?> </p>
						
						<!-- FILTRO CONTEÚDO DA LOJA  -->
						<div class="filtroConteudo-loja">						
							<div class="row">
								<style>
								.foto-produto  .product_type_variable{
									display: none!important;

								}
								.term-description{
									display: none;
								}
							  </style>
								<div class="col-md-6">
									<!-- CATEGORYA ATUAL -->
									<?php 	
										$categoria = get_queried_object();
										if ($categoria->name == "product") {
											
										
											$categorianame = "Produtos";
										}else{
											$categorianame = $categoria->name;
										}
									?>
									<span><?php echo $categorianame; ?></span>

									<!-- <p>1-16 de 78 produtos relacionados</p>	 -->
									<div class="form-group resultados">
										<?php
											/**
											 * woocommerce_before_shop_loop hook.
											 *
											 * @hooked woocommerce_result_count - 20
											 * @hooked woocommerce_catalog_ordering - 30
											 */
											do_action( 'woocommerce_before_shop_loop' );
										?>
									</div>								
								</div>
								
								<div class="col-md-6 correcaoX">
									<!-- FORMA DE VIZUALIZAÇÃO  -->
									<div id="lista" class="icon"><i class="fa fa-th-list" aria-hidden="true"></i></div>
									<div id="grade" class="icon"><i class="fa fa-th-large" aria-hidden="true"></i></div>
								
									<div class="form-group select">
									<style>
										
										.select .woocommerce-result-count{
											display: none!important;
										}
									</style>
										<?php
											/**
											 * woocommerce_before_shop_loop hook.
											 *
											 * @hooked woocommerce_result_count - 20
											 * @hooked woocommerce_catalog_ordering - 30
											 */
											do_action( 'woocommerce_before_shop_loop' );
										?>
									</div>
									<!-- SELECT -->
									<!-- <div class="form-group">
										<label class="hidden">selecione</label>
										<select class="form-control" id="sel1">
											<option>Em Destaque</option>
											<option>Brasil</option>
											<option>Austrália</option>
											<option>Alemanha</option>
										</select>
									</div> -->
								</div>
							
							</div>					
						</div>

						<!-- PRODUTOS DA LOJA -->
						<div class="produtos-loja">			
								<div class="carrossel-produtos-destaque" >
								
						<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						//do_action( 'woocommerce_before_main_content' );
						?>

							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

								<!-- <strong class="tituloPesquisa"><?php woocommerce_page_title(); ?></strong> -->

							<?php endif; ?>

							<?php
								/**
								 * woocommerce_archive_description hook.
								 *
								 * @hooked woocommerce_taxonomy_archive_description - 10
								 * @hooked woocommerce_product_archive_description - 10
								 */
								do_action( 'woocommerce_archive_description' );
							?>

							<?php if ( have_posts() ) : ?>

								

								<?php woocommerce_product_loop_start(); ?>

									<?php woocommerce_product_subcategories(); ?>

									<?php while ( have_posts() ) : the_post(); ?>

										<?php wc_get_template_part( 'content', 'product' ); ?>

									<?php endwhile; // end of the loop. ?>

								<?php woocommerce_product_loop_end(); ?>

								<?php
									/**
									 * woocommerce_after_shop_loop hook.
									 *
									 * @hooked woocommerce_pagination - 10
									 */
									do_action( 'woocommerce_after_shop_loop' );
								?>

							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

								<?php wc_get_template( 'loop/no-products-found.php' ); ?>

							<?php endif; ?>

						<?php
							/**
							 * woocommerce_after_main_content hook.
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action( 'woocommerce_after_main_content' );
						?>

						<?php
							/**
							 * woocommerce_sidebar hook.
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							//do_action( 'woocommerce_sidebar' );
						?>
							</div>
						</div> 
													

					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer( 'shop' ); ?>
