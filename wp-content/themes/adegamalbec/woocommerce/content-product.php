<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
global $product;
 //var_dump($product);
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
?>




<li class="item lista-grade" <?php post_class( $classes ); ?>>

	<a href="<?php the_permalink(); ?>">
		<div class="foto-produto">
			<!-- DESCONTO PORCENTAGEM -->
			<?php

				$product->get_price_html();
				$precoatual = $product->price;
				// $precoantigo = $product->regular_price;
				$valor = explode(";", $product->get_price_html());
				$precoantigo = $valor[2];


				$desconto = 0;
				if ($precoantigo != 0)
				$desconto = (($precoatual * 100) / $precoantigo) - 100;

			if ($product->is_on_sale() == "true") {
				# code...

			?>


			<span class="desconto-porcetagem"><?php echo intval($desconto); ?>%</span>
			<?php }; ?>
			<?php
				/**
				 * woocommerce_before_shop_loop_item hook.
				 *
				 * @hooked woocommerce_template_loop_product_link_open - 10
				 */
				//do_action( 'woocommerce_before_shop_loop_item' );

				/**
				 * woocommerce_before_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
			?>

			<!-- <button class="comprar"><img src="<?php bloginfo('template_directory'); ?>/img/sacola.png" alt="">Adicionar ao carrinho</button>
			<?php 	do_action( 'woocommerce_after_shop_loop_item' ); ?> -->
			<?php
			if($product->product_type == "variable"){
				$prodVars = $product->get_available_variations();
				$urlBlog = get_template_directory_uri();
				foreach ($prodVars as $prodVar) {
					if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {


						$unId = $prodVar['variation_id'];
						echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><img src="'.$urlBlog.'/img/sacola.png" alt="icone Sacola de compras" id="sacolaCompras">Adicionar ao carrinho</a>';
					}
				}
			}else{
				$unId = $product->id;
				$urlBlog = get_template_directory_uri();
				if ($product->is_in_stock() == true && $product->get_stock_quantity() > 0) {
					echo '<a class="buy areaImgComprar link-botao-comprar" href="#" data-unId="'.$unId.'"><img src="'.$urlBlog.'/img/sacola.png" alt="icone Sacola de compras" id="sacolaCompras">Adicionar ao carrinho</a>';
				}
			}

			?>
		</div>
	</a>
<div class="conteudo">
	<div class="descricao-produto">
		<?php
			/**
			 * woocommerce_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );

		?>
	</div>

	<!-- PREÇO -->
	<p class="preco">R$ <?php echo number_format((float)$product->get_price(),2, ',', '.'); ?></p>
	<?php
	$price   = $product->get_price();
	$price3x = $price / 3;
	$price2x = $price / 2;
	?>

	<?php if ($price >= 120): ?>
		<p class="promocao"><span>ou</span>3X <?php echo number_format($price3x,2,",","." ); ?></p>
	<?php elseif($price >= 80): ?>
		<p class="promocao"><span>ou</span>2X <?php echo number_format($price2x,2,",","." ); ?></p>
	<?php else: ?>
		<p class="promocao" style="height: 20px;"></p>
	<?php endif; ?>
	<div class="estrela">
		<?php
			 $average = $product->get_average_rating();
			 $avaliacao = explode(".", $average);


			 if ($avaliacao[0] == "5") {

			 	echo'
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>

		 			';
			 }elseif ($avaliacao[0] == "4") {
			 	echo'
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star-o" aria-hidden="true"></i>
		 			';
			 }elseif ($avaliacao[0] == "3") {
			 	echo'
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>


			 			<i class="fa fa-star-o" aria-hidden="true"></i>
			 			<i class="fa fa-star-o" aria-hidden="true"></i>
		 			';
			 }elseif ($avaliacao[0] == "2") {
			 	echo'
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>


			 			<i class="fa fa-star-o" aria-hidden="true"></i>
			 			<i class="fa fa-star-o" aria-hidden="true"></i>
		 			';
			 }elseif ($avaliacao[0] == "1") {
			 	echo'
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>
			 			<i class="fa fa-star" aria-hidden="true"></i>

			 			<i class="fa fa-star-o" aria-hidden="true"></i>

		 			';
			 }elseif ($avaliacao[0] == "0") {
			 	echo'
			 		<i class="fa fa-star-o" aria-hidden="true"></i>
			 		<i class="fa fa-star-o" aria-hidden="true"></i>
			 		<i class="fa fa-star-o" aria-hidden="true"></i>
			 		<i class="fa fa-star-o" aria-hidden="true"></i>
			 		<i class="fa fa-star-o" aria-hidden="true"></i>

		 			';
			 }

		?>

	</div>

	<div class="descricao-completa">
		<p><?php
				$textoresumido =  $product->post->post_content;

				$textocurto = substr($textoresumido, 0, 500).'...';
				echo $textocurto;

		?></p>
	</div>
	<?php
	if($product->product_type == "variable"){
		$prodVars = $product->get_available_variations();
		$result = woocommerce_get_product_terms($product->id, 'pa_quantidade', 'names');
		foreach ($prodVars as $prodVar) {
			if ($prodVar["is_in_stock"] == true && $prodVar["attributes"]["attribute_pa_quantidade"] != "unidade") {
				$sixId = $prodVar['variation_id'];

				foreach ($result as $result) {
				 	$variacao = $result;

					if ($variacao != "Unidade") {
						$quantidadeCaixa = $variacao;
					}else{
						$unidade = $variacao;
					}
				 }


				$str = $quantidadeCaixa;
				$qTD = filter_var($str, FILTER_SANITIZE_NUMBER_INT);

				$sixDiv = $prodVar['display_price'] / $qTD;

				$sixDiv = money_format('%.2n', $sixDiv);

				echo '<a class="buy6 adicionar-aocarrinho" href="#" data-sixId="'.$sixId.'">compre <b>'.$qTD.'un</b> pague <b>R$'.$sixDiv.'</b>/un</a>';

			} else if($prodVar["is_in_stock"] == false && $prodVar["attributes"]["attribute_pa_quantidade"] == "unidade") {
				echo '<p class="esgotado text-center"><i class="fa fa-times"></i> Esgotado</p>';
			}
		}
	}else{
		$quantidadeProduto = $product->get_stock_quantity();
		if ($quantidadeProduto = 0 || $quantidadeProduto == null) {
			echo '<p class="esgotado text-center"><i class="fa fa-times"></i> Esgotado</p>';
		}
	}

	?>
</div>
<?php
	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */

	?>

	</li>



