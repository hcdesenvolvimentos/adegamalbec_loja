<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>
	<!-- PÁGINA DE CARRINHO -->
	<div class="pg pg-carrinho-loja internas" style="display:;">
		<div class="container">
			<div class="meucarrinho">
				<span class="titulo">Minha sacola <p>ambiente 100% seguro<img src="<?php bloginfo('template_directory'); ?>/img/6.png" alt=""></p></span>
			</div>


				<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

					<?php do_action( 'woocommerce_before_cart_table' ); ?>
					<div class="carrinho table-responsive">
						<table class="table " cellspacing="0">
							<thead>
								<tr>
									<th class="vazio">&nbsp;</th>
									<th class="v-azio">&nbsp;</th>
									<th class="nome"><?php _e( 'Product', 'woocommerce' ); ?></th>
									<th class="title-quantidade"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
									<th class="title-total-unitario">	Total Unitário</th>
									<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php do_action( 'woocommerce_before_cart_contents' ); ?>

								<?php
								foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
									$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
									$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

									if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
										?>
										<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

											<td class="remover">
												<?php
													echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
														'<a href="%s"  title="%s" data-product_id="%s" data-product_sku="%s">X</a>',
														esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
														__( 'Remove this item', 'woocommerce' ),
														esc_attr( $product_id ),
														esc_attr( $_product->get_sku() )
													), $cart_item_key );
												?>
											</td>

											<td class="product-thumbnail">
												<?php
													$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

													if ( ! $_product->is_visible() ) {
														echo $thumbnail;
													} else {
														printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
													}
												?>
											</td>

											<td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
												<?php
													if ( ! $_product->is_visible() ) {
														echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
													} else {
														echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s"><p>%s</p></a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
													}
													// VERIFICAÇÃO  CAIXA COM 6 UNIDADES OU UNIDADE
													// IF VERIFICANDO SE O PRODUTO É SIMPLES OU VARIÁVEL 
													if ($_product->product_type == "variation") {
														
														$id = $_product->variation_id;
														$variacao = wc_get_product($id);
														$variacao->get_formatted_name();
														$nomeVariacao = $variacao->variation_data;														
														$nomeVariacao["attribute_pa_quantidade"];
														//var_dump($nomeVariacao["attribute_pa_quantidade"]);

														
														$tipoCaixa = explode("-", $nomeVariacao["attribute_pa_quantidade"]);
// echo "<pre>";
// 																	var_dump($tipoCaixa);
// 																	echo "</pre>";
														if ($nomeVariacao["attribute_pa_quantidade"] == "caixa-com-".$tipoCaixa[2]."-unidades") {
															echo "Caixa com ".$tipoCaixa[2]." unidades";
														}else{
															echo "Unidade";
														}

													}else{
														echo "Unidade";
													}
													
												
													// Meta data
													echo WC()->cart->get_item_data( $cart_item );

													// Backorder notification
													if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
														echo '<p>' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
													}
												?>
											</td>

											<td class="valor-quantidade" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
												<?php
													if ( $_product->is_sold_individually() ) {
														$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
													} else {
														$product_quantity = woocommerce_quantity_input( array(
															'input_name'  => "cart[{$cart_item_key}][qty]",
															'input_value' => $cart_item['quantity'],
															'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
															'min_value'   => '0'
														), $_product, false );
													}

													echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
												?>
											</td>

											<td class="valor-unitario" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
												<?php
													echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
												?>
											</td>



											<td class="valor-total" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
												<?php
													echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
												?>
											</td>
										</tr>
										<?php
									}
								}

								do_action( 'woocommerce_cart_contents' );
								?>


								<?php do_action( 'woocommerce_after_cart_contents' ); ?>
							</tbody>
						</table>


					</div>

					<div class="row info-produto">
				<div class="col-md-4">
					<div class="frete">
						<span>calcular frete</span>

						<!-- ÁREA SIMULADOR DE FRETE -->
						<form class="woocommerce-shipping-calculator" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

							<section class="" style="display:block;">

									<p class="form-row form-row-wide" style="display: none" id="calc_shipping_country_field">
										<select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state" rel="calc_shipping_state">
											<option value=""><?php _e( 'Select a country&hellip;', 'woocommerce' ); ?></option>
											<?php
											foreach( WC()->countries->get_shipping_countries() as $key => $value )
												echo '<option value="' . esc_attr( $key ) . '"' . selected( WC()->customer->get_shipping_country(), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
											?>
										</select>
									</p>

									<p class="form-row form-row-wide" style="display: none" id="calc_shipping_state_field">
										<?php
										$current_cc = WC()->customer->get_shipping_country();
										$current_r  = WC()->customer->get_shipping_state();
										$states     = WC()->countries->get_states( $current_cc );

											// Hidden Input
										if ( is_array( $states ) && empty( $states ) ) {

											?><input type="hidden" name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>" /><?php

											// Dropdown Input
										} elseif ( is_array( $states ) ) {

											?><span>
											<select name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>">
												<option value=""><?php _e( 'Select a state&hellip;', 'woocommerce' ); ?></option>
												<?php
												foreach ( $states as $ckey => $cvalue )
													echo '<option value="' . esc_attr( $ckey ) . '" ' . selected( $current_r, $ckey, false ) . '>' . __( esc_html( $cvalue ), 'woocommerce' ) .'</option>';
												?>
											</select>
										</span><?php

										// Standard Input
										} else {

											?><input type="text" class="input-text" value="<?php echo esc_attr( $current_r ); ?>" placeholder="<?php esc_attr_e( 'State / county', 'woocommerce' ); ?>" name="calc_shipping_state" id="calc_shipping_state" /><?php

										}
										?>
									</p>

									<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_city', false ) ) : ?>

										<p class="form-row form-row-wide" id="calc_shipping_city_field">
											<input type="text" class="input-text" value="<?php echo esc_attr( WC()->customer->get_shipping_city() ); ?>" placeholder="<?php esc_attr_e( 'City', 'woocommerce' ); ?>" name="calc_shipping_city" id="calc_shipping_city" />
										</p>

									<?php endif; ?>

									<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_postcode', true ) ) : ?>

									<!-- 	<p class="form-row form-row-wide" id="calc_shipping_postcode_field"> -->
											<input type="text"  value="<?php echo esc_attr( WC()->customer->get_shipping_postcode() ); ?>" placeholder="<?php esc_attr_e( 'Postcode / ZIP', 'woocommerce' ); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
										<!-- </p>-->
									<?php endif; ?>

								<!-- <p> --><button type="submit" name="calc_shipping" value="1" ><?php _e( 'Calcular', 'woocommerce' ); ?></button><!-- </p> -->

								<?php wp_nonce_field( 'woocommerce-cart' ); ?>
							</section>

						

					</div>
				</div>

				<div class="col-md-4">
					<div class="cupom">
					<?php if ( wc_coupons_enabled() ) { ?>
								<div class="coupon">

									 <span>cupom de desconto</span><input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
									<button type="submit"  name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>">aplicar</button>

									<?php do_action( 'woocommerce_cart_coupon' ); ?>
								</div>
							<?php } ?>



							<?php do_action( 'woocommerce_cart_actions' ); ?>

							<?php wp_nonce_field( 'woocommerce-cart' ); ?>

						<!-- <input placeholder="Digite seu cupom" type="text"> -->
					</div>
				</div>

				<div class="col-md-4">
					<div class="subtotal">
						<?php do_action( 'woocommerce_cart_collaterals' ); ?>
						<div class="row botoes">
							<a href="<?php echo esc_url( wc_get_checkout_url() ) ;?>" id="continuar">Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>

			</div>
			<div class="row botoes">
				<a href="<?php echo home_url('/loja'); ?>" id="comprar">Continuar comprando </a>
				<!-- <a href="<?php echo home_url('/carrinho'); ?>" id="comprar">Atualizar Carrinho</a> -->
				 <input type="submit" class="button atualizarCarrinho" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
			</div>
					 <?php do_action( 'woocommerce_after_cart_table' ); ?> 

				</form>


		</div>
	</div>
<?php do_action( 'woocommerce_after_cart' ); ?>
