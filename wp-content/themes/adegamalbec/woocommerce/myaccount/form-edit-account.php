<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="container"><?php wc_print_notices(); ?></div>	
<!-- PÁGINA DE DADOS CADASTRADO DE ENDEREÇO-->
	<div class="pg pg-dados-endereco internas" >
		<div class="container">
			
			<!-- DADOS CADASTRAIS -->
			<div class="dados">
				<span class="titulo">meu cadastro</span>

				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3 side">
						<div class="sidebar-cadastro">
							<a href="<?php echo home_url('/minha-conta/'); ?>"><div class="foto-perfil"><img src="<?php bloginfo('template_directory'); ?>/img/user.png" alt=""></div></a>
							<span>
								
							<?php
								printf(
									__( '%1$s' ) . ' ',
									
									$current_user->display_name,
									
									wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
								);
									
							?>
							</span>
							<?php 

									
							printf(
								__( ' <a href="%2$s">Sair</a>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);
							 ?>
							

							<div class="menu">
								<a href="<?php echo home_url('/minha-conta/edit-account/'); ?>">Meus dados cadastrais</a>
								<a href="<?php echo home_url('/minha-conta/edit-address/'); ?>">Meus endereços</a>								
								<small  id="pedidos">Meus pedidos</small>						
								
								
							</div>
						</div>
					</div>




					<!-- FORMULÁRIO CADASTRO -->
					<div class="col-md-9">
						<div id="form-pedidos" style="display: none">
							<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
						</div>
						
						<div id="caixa-dados">
							<span class="subtitulo">meus dados cadastrais</span>
							<div class="form">
								<form class="edit-account" action="" method="post">

									<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

									<div class="form-group">
										<label for="nome">Nome*</label>
										<input type="text" class="input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
									</div>

									<div class="form-group">
										<label for="Sobrenome">Sobrenome*</label>
										<input type="text" class="input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
									</div>
									<div class="clear"></div>

									<div class="form-group">
										<label for="Email">Email*</label>
										<input type="email" class="input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
									</div>

									<fieldset>
										<legend><?php _e( 'Password Change', 'woocommerce' ); ?></legend>

										<div class="form-group">
											<label for="Senha">Senha*</label>
											<input type="password" class="input-text" name="password_current" id="password_current" />
										</div>
										<div class="form-group">
											<label for="password_1"><?php _e( 'New Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
											<input type="password" class="input-text" name="password_1" id="password_1" />
										</div>
										<div class="form-group">
											<label for="password_2"><?php _e( 'Confirm New Password', 'woocommerce' ); ?></label>
											<input type="password" class="input-text" name="password_2" id="password_2" />
										</div>
									</fieldset>
									<div class="clear"></div>

									<?php do_action( 'woocommerce_edit_account_form' ); ?>

									<div class="form-group">
										<?php wp_nonce_field( 'save_account_details' ); ?>
										<label></label>
										<button type="submit"  name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>">Salvar Alterações</button>
										
										<input type="hidden" name="action" value="save_account_details" />
									
									</div>
									<?php do_action( 'woocommerce_edit_account_form_end' ); ?>

								</form>	
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
<script>
	$(document).ready(function() {
		
			$('#pedidos').click(function(e){ 
				 $('#form-pedidos').css({"display":"block"})
				$('#caixa-dados').css({"display":"none"})
				
			});
			$('.foto-perfil').click(function(e){ 
			
				$('.myaccount_user').css({"display":"block"})

			});
		});
</script>