<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version   2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="container"><?php wc_print_notices(); ?></div>	



<?php if ( $notes = $order->get_customer_order_notes() ) :
	?>
	<h2><?php _e( 'Order Updates', 'woocommerce' ); ?></h2>
	<ol class="commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="comment note">
			<div class="comment_container">
				<div class="comment-text">
					<p class="meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
					<div class="description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</div>
	  				<div class="clear"></div>
	  			</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
	<?php
endif;
?>
	<!-- PÁGINA DE DADOS CADASTRADO TODOS OS PEDIDOS-->
	<div class="pg pg-dados-pedido internas">
		<div class="container">
			
			<!-- DADOS CADASTRAIS -->
			<div class="dados">
				<span class="titulo">meu cadastro</span>

				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3 side">
						<div class="sidebar-cadastro">
							<a href="<?php echo home_url('/minha-conta/'); ?>"><div class="foto-perfil"><img src="<?php bloginfo('template_directory'); ?>/img/user.png" alt=""></div></a>
							<span>
								
							<?php
								printf(
									__( '%1$s' ) . ' ',
									
									$current_user->display_name,
									
									wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
								);
									
							?>
							</span>
							<?php 

									
							printf(
								__( ' <a href="%2$s">Sair</a>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);
							 ?>
							

							<div class="menu">
								<a href="<?php echo home_url('/minha-conta/edit-account/'); ?>">Meus dados cadastrais</a>
								<a href="<?php echo home_url('/minha-conta/edit-address/'); ?>">Meus endereços</a>								
								<small  id="pedidos">Meus pedidos</small>						
								
								
							</div>
						</div>
					</div>

					
					
					<!-- FORMULÁRIO CADASTRO -->
					<div class="col-md-9">

						<div id="form-pedidos" style="display: none">
							<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
						</div>
						
						<div id="caixa-produto">
							<span class="subtitulo">meus pedidos</span>

							<div class="form-pedidos">						
								<div class="row">
									<div class="col-md-5">																		
										<div class="col-md-12 col-sm-6 col-xs-12 infopedido">
											<span class="pedido">Pedido 
												<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>" style="color:#000;">
													<?php echo _x( '<strong>#', 'hash before order number','</strong>', 'woocommerce' ) . $order->get_order_number(); ?>
												</a>
											</span>
											<span><time datetime="<?php echo date( 'Y-m-d', strtotime( $order->order_date ) ); ?>" title="<?php echo esc_attr( strtotime( $order->order_date ) ); ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></time></span>
											<span>Status<?php echo ":",  wc_get_order_status_name( $order->get_status() ); ?></span>
											<?php
													$formaenvio = explode(" ",sprintf( _n( '%s for %s item', '%s for %s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ));
													
												?>			
											<span>Valor total:<?php echo $formaenvio['0'], $formaenvio['1'] ;?></span>
																			
											<span>
												<?php
													foreach ( $order->get_order_item_totals() as $key => $total ) {
												?>
													<?php  
														$formapagamento = $total['value']; 

														
													?>
													
													<?php 
														if ($formapagamento == 'PagSeguro') {
															$forma = $formapagamento;
															# code...
														}
													 ?>
													 <?php } 
											 	?>
												 Forma de Pagamento: <?php echo $forma ; ?>
											</span>
											<span>
												<?php
														foreach ( $order->get_order_item_totals() as $key => $total ) {
													?>
														<?php  
															$formasenvio = $total['value']; 

															
														?>
														
														<?php 
															if ($formasenvio == '<span class="amount">&#82;&#36;14,80</span>&nbsp;<small class="shipped_via">via PAC</small>') {
																 $formaenvio = $formaenvio;
																 $formaenvio = explode(" ",$formasenvio);
																

																echo 'Fomra de Envio:'. $formaenvio['3'];
															}
														 ?>
													 	<?php } 
												 ?>
												
											</span>
											<span>Código de rastreio:DU12340210BR</span>
											<a href="">Rastrear Pedido</a>
											
										</div>																
										
										<div class="col-md-12 col-sm-6 col-xs-12 entrega">	
											<?php 
												$endereco=  $order->get_shipping_address();
												
												

											 ?>								
											<span class="pedido">Endereço e contato de entrega</span>
											<span><?php echo $endereco ?></span>

										</div>	
									</div>

									<div class="col-md-7">

										<?php do_action( 'woocommerce_view_order', $order_id ); ?>																
																					
										
										<div class="col-md-12 col-sm-6 col-xs-12 total">
											
											<div class="col-xs-6 info">
												
													<p>Subtotal:</p>
													<p>Frete:</p>
													<p>Pagamento:</p>
													<p><b>Total:</b></p>
												
												<hr>
													
											</div>

											<div class="col-xs-6 valores">
												<?php
													foreach ( $order->get_order_item_totals() as $key => $total ) {
												?>
													<p><?php echo $total['value']; ?></p>
												<?php } ?>	
											</div>

										</div>
									</div>
								</div>
									<p class="order-info"><?php printf( __( 'Order #<mark class="order-number">%s</mark> was placed on <mark class="order-date">%s</mark> and is currently <mark class="order-status">%s</mark>.', 'woocommerce' ), $order->get_order_number(), date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ), wc_get_order_status_name( $order->get_status() ) ); ?></p>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function() {
		
			$('#pedidos').click(function(e){ 
				 $('#form-pedidos').css({"display":"block"})
				$('#caixa-produto').css({"display":"none"})
				
			});
			$('.foto-perfil').click(function(e){ 
			
				$('.myaccount_user').css({"display":"block"})

			});
		});
</script>
