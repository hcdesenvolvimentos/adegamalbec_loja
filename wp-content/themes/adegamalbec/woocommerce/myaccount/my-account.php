<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
 global $woocommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="container"><?php wc_print_notices(); ?></div>	
	<!-- FORMULÁRIO CADASTRO -->
	<style>
		.info{
			display: block;
			margin-top: 50px;
		}
		.info a{
			display: block;

		}
		.nome{
			display: block;
			text-align: center;
		}
	</style>

	<!-- PÁGINA DE DADOS CADASTRADO -->
	<div class="pg pg-dados-cadastrados internas" style="display: ;">
		<div class="container">
			
			<!-- DADOS CADASTRAIS -->
			<div class="dados">
				<span class="titulo">meu cadastro</span>

				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3 side">
						<div class="sidebar-cadastro">
							<a href="<?php echo home_url('/minha-conta/'); ?>"><div class="foto-perfil"><img src="<?php bloginfo('template_directory'); ?>/img/user.png" alt=""></div></a>
							<span>
								
							<?php
								printf(
									__( '%1$s' ) . ' ',
									
									$current_user->display_name,
									
									wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
								);
									
							?>
							</span>
							<?php 

									
							printf(
								__( ' <a href="%2$s">Sair</a>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);
							 ?>
							

							<div class="menu">
								<a href="<?php echo home_url('/minha-conta/edit-account/'); ?>">Meus dados cadastrais</a>
								<a href="<?php echo home_url('/minha-conta/edit-address/'); ?>">Meus endereços</a>								
								<small  id="pedidos">Meus pedidos</small>						
								
								
							</div>
						</div>
					</div>
					
					<div class="col-md-9">
						
						<p class="myaccount_user">
							<?php
							printf(
								__( '<span class="nome">Olá  <strong>%1$s</strong> (não é %1$s? <a href="%2$s">Sair</a>).</span>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);

							printf( __( '<span class="info">No painel da sua conta você pode ver seus pedidos recentes, gerenciar seus endereços de entrega e cobrança <a href="%s">editar sua senha e detalhes da conta.</a>.</span>', 'woocommerce' ),
								wc_customer_edit_account_url()
							); 	
							?>
							<?php do_action( 'woocommerce_before_my_account' ); ?>				

						</p>
						
						<div id="form-pedidos" style="display: none">
							<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
						</div>
						
						<?php do_action( 'woocommerce_after_my_account' ); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	<script>
		
		$(document).ready(function() {
		
			$('#pedidos').click(function(e){ 
				 $('#form-pedidos').css({"display":"block"})
				$('.myaccount_user').css({"display":"none"})
				$('.enderecos').css({"display":"none"})
			});
			$('.foto-perfil').click(function(e){ 
				$('.enderecos').css({"display":"none"})
				$('.myaccount_user').css({"display":"block"})

			});
		});
	</script>