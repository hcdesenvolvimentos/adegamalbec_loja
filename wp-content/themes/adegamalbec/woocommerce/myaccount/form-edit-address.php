<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$page_title   = ( $load_address === 'billing' ) ? __( 'Billing Address', 'woocommerce' ) : __( 'Shipping Address', 'woocommerce' );
?>

<div class="container"><?php wc_print_notices(); ?></div>	


<!-- PÁGINA DE DADOS CADASTRADO -->
	<div class="pg pg-dados-cadastrados internas" style="display: ;">
		<div class="container">
			
			<!-- DADOS CADASTRAIS -->
			<div class="dados">
				<span class="titulo">meu cadastro</span>

				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3 side">
						<div class="sidebar-cadastro">
							<a href="<?php echo home_url('/minha-conta/'); ?>"><div class="foto-perfil"><img src="<?php bloginfo('template_directory'); ?>/img/user.png" alt=""></div></a>
							<span>
								
							<?php
								printf(
									__( '%1$s' ) . ' ',
									
									$current_user->display_name,
									
									wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
								);
									
							?>
							</span>
							<?php 

									
							printf(
								__( ' <a href="%2$s">Sair</a>', 'woocommerce' ) . ' ',
								
								$current_user->display_name,
								
								wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
							);
							 ?>
							

							<div class="menu">
								<a href="<?php echo home_url('/minha-conta/edit-account/'); ?>">Meus dados cadastrais</a>
								<a href="<?php echo home_url('/minha-conta/edit-address/'); ?>">Meus endereços</a>								
								<small  id="pedidos">Meus pedidos</small>						
								
								
							</div>
						</div>
					</div>


					<!-- FORMULÁRIO CADASTRO -->
					<div class="col-md-9">
						<div id="form-pedidos" style="display: none">
							<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
						</div>
						
						<div id="caixa">
							<span class="subtitulo"><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title ); ?></span>
							<div class="form-endereco">
							<div class="form">
								
								<?php if ( ! $load_address ) : ?>

										<?php wc_get_template( 'myaccount/my-address.php' ); ?>

									<?php else : ?>
									
										<form method="post">

											

											<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

											<?php foreach ( $address as $key => $field ) : ?>

												<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

											<?php endforeach; ?>

											<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

											<div class="form-group">
												<label for="	"></label>
												<button type="submit"  name="save_address" value="<?php esc_attr_e( 'Save Address', 'woocommerce' ); ?>">Salvar alterações</button>
												<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
												<input type="hidden" name="action" value="edit_address" />
											</div>

										</form>
									</div>
									<?php endif; ?>

								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		
		$(document).ready(function() {
		
			$('#pedidos').click(function(e){ 
				  $('#form-pedidos').css({"display":"block"})
				 $('#caixa').css({"display":"none"})
				// alert('oi');
				
			});
			$('.foto-perfil').click(function(e){ 
				$('.enderecos').css({"display":"none"})
				$('.myaccount_user').css({"display":"block"})

			});
		});
	</script>