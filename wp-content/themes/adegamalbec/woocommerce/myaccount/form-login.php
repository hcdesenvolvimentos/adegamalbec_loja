<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="container"><?php wc_print_notices(); ?></div>

<?php do_action( 'woocommerce_before_customer_login_form' );?>


<?php if( isset( $_GET['action']) && $_GET['action'] == "register") : ?>

	<!-- PÁGINA DE CADASTRO -->
	<div class="pg pg-cadastro internas" style="display:  ;">
		<div class="container">
			<span class="titulo">cadastrar</span>
			<div class="formulario-cadastro">
				<?php   $email = $_GET['email']; ?>
				<div class="form">
					<form method="post" class="form-cadastro" action="<?php echo home_url('minha-conta'); ?>">

						<?php do_action( 'woocommerce_register_form_start' ); ?>


							<div id="billing_persontype_field">
									<label for="" style="width: 100px" class="label-botao"></label>
									<a id="fisica">Pessoa Física </a>
									<a id="juridica">Pessoa Jurídica </a>

							</div>

						<div class="form-group">
							<label for="reg_billing_first_name"><?php _e( 'Nome*', 'woocommerce' ); ?> </label>
							<input type="text" class="form-control" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
						</div>

						<div class="form-group">
							<label for="reg_billing_last_name"><?php _e( 'Sobrenome*', 'woocommerce' ); ?> </label>
							<input type="text" class="form-control" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
						</div>

						<div class="form-group" id="cpf" style="display: none;">
							<div  id="billing_cpf_field">
								<div  style="padding-right: 20px;">
									<label for="billing_cpf" class="" style="display:inline-block!important">CPF *</label>
									<input type="text" class="form-control " name="billing_cpf" id="billing_cpf">
								</div>
							</div>
						</div>

						<div class="form-group" id="razao-social" style="display: none;">
							<div class="validate-required" id="billing_company_field">
								<label for="billing_company" style="display:inline-block!important" >Razão Social* </label>
								<input type="text" class="form-control " name="billing_company" id="billing_company" value="">
							</div>
						</div>

						<div class="form-group" id="cnpj" style="display: none;">
							<div class="validate-required" id="billing_cnpj_field" >
								<label for="billing_cnpj" class="">CNPJ*</label>
								<input type="text" class="form-control " name="billing_cnpj" id="billing_cnpj" value="">
							</div>
						</div>

						<div class="form-group" id="iEstadual" style="display: none;">
							<div class="validate-required" id="billing_ie_field">
								<label for="billing_ie" class="">Inscrição Estadual* </label>
								<input type="text" class="form-control " name="billing_ie" id="billing_ie" value="">
							</div>
						</div>

						<div class="form-group">

								<div style="padding-right: 20px;">
									<label for="reg_billing_phone"><?php _e( 'Telefone*', 'woocommerce' ); ?> </label>
									<input type="text" class="form-control" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
								</div>

						</div>

						<div class="form-group">

								<div  style="padding-right: 20px;">
									<label for="reg_email"><?php _e( 'E-mail*', 'woocommerce' ); ?> </label>
									<input type="email" class="form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_GET['email'] ) ) echo esc_attr( $_GET['email'] ); ?>" />
								</div>

						</div>

						<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

						<div class="form-group">

								<label for="reg_password"><?php _e( 'Senha*', 'woocommerce' ); ?> </label>
								<input type="password" class="form-control" name="password" id="reg_password" />

						</div>

						<div class="form-group">

								<label for="conf_password">Confirmar senha* </label>
								<input type="password" class="form-control" name="password2" id="conf_password" />

						</div>
						<?php //endif; ?>

						<!-- Spam Trap -->
						<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

						<?php do_action( 'woocommerce_register_form' ); ?>

						<?php do_action( 'register_form' ); ?>

						<div class="form-group">
						<label ></label>
							<?php wp_nonce_field( 'woocommerce-register' ); ?>
							<button type="submit" name="register" value="<?php _e( 'Cadastrar', 'woocommerce' ); ?>" >Cadastrar</button>
						</div>

						<?php do_action( 'woocommerce_register_form_end' ); ?>

					</form>
				</div>
			</div>
		</div>
	</div>



<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
	<!-- PÁGINA DE LOGIN -->
	<div class="pg pg-login">
		<div class="container">
			<div class="row correcao-xy">
				<div class="col-md-8 col-md-offset-2 correcao-xy">
					<div class="titulo text-center">
						<span>Preencha o formulário abaixo com seus dados</span>
					</div>
					<div class="cadastrar row correcao-xy">
						<div class="form-cad row">

							<form method="post" class="">

								<?php do_action( 'woocommerce_register_form_start' ); ?>

								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

									<!-- <p class="form-row form-row-wide">
										<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
									</p> -->

								<?php //endif; ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
								</div>

								<div class="col-md-12 correcao-x" id="billing_persontype_field">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="billing_persontype" class="">Tipo de Pessoa <span class="required">*</span></label>
										<select name="billing_persontype" id="billing_persontype" class="select form-control" placeholder="">
											<option value="0">Selecionar</option>
											<option value="1">Pessoa Física</option>
											<option value="2">Pessoa Jurídica</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div  id="billing_cpf_field" style="display: ;">
										<div  style="padding-right: 20px;">
											<label for="billing_cpf" class="" style="display:inline-block!important">CPF *</label>
											<input type="text" class="input-text " name="billing_cpf" id="billing_cpf">
										</div>
									</div>
								</div>

								<div class="col-md-12 correcao-x validate-required" id="billing_company_field" style="display: ;">
									<label for="billing_company" class="">Razão Social <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_company" id="billing_company" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_cnpj_field" style="display:">
									<label for="billing_cnpj" class="">CNPJ <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_cnpj" id="billing_cnpj" value="">
								</div>

								<div class="col-md-6 correcao-x validate-required" id="billing_ie_field" style="display: ;">
									<label for="billing_ie" class="">Inscrição Estadual <span class="required">*</span></label>
									<input type="text" class="input-text " name="billing_ie" id="billing_ie" value="">
								</div>
								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
									</div>
								</div>

								<div class="col-md-12 correcao-x">
									<div class="col-md-6" style="padding-right: 20px;">
										<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
									</div>
								</div>


								<?php //if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

								<div class="col-md-6 correcao-x">
									<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
									<input type="password" class="input-text" name="password" id="reg_password" />
								</div>

								<div class="col-md-6 correcao-x">
									<label for="conf_password">Confirmar senha <span class="required">*</span></label>
									<input type="password" class="input-text" name="password2" id="conf_password" />
								</div>

								<?php //endif; ?>

								<!-- Spam Trap -->
								<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

								<?php do_action( 'woocommerce_register_form' ); ?>

								<?php do_action( 'register_form' ); ?>

								<div class="col-md-8 correcao-x">
									<?php wp_nonce_field( 'woocommerce-register' ); ?>
									<input type="submit" class="button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
								</div>

								<?php do_action( 'woocommerce_register_form_end' ); ?>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
<?php else : ?>

	<!-- PÁGINA DE LOGIN  -->
	<div class="pg pg-login-loja internas" >
		<div class="container">
			<span class="titulo">Identificação</span>

				<div class="area-login">
					<div class="row">
						<!-- ÁREA LOGIN -->
						<div class="col-md-6">
							<div class="formulario login">
								<form method="post">
								<p>Já tenho login</p>
								<span>Login</span>
									<?php do_action( 'woocommerce_login_form_start' ); ?>
									<!-- INPUT EMAIL -->
									<div class="form-group">

										<label class="hidden" placeholder="E-mail" for="username"><?php _e( 'E-MAIL', 'woocommerce' ); ?> <strong>*</strong></label>

										<input type="text"  placeholder="E-mail"  name="username" id="username" onBlur="if(this.value=='')this.value=''" onFocus="if(this.value=='seu email...')this.value='' " />

									</div>
									<!-- INPUT SENHA -->
									<div class="form-group">

										<label class="hidden" for="password"><?php _e( 'SENHA', 'woocommerce' ); ?> <strong>*</strong></label>

										<input class="input-text" placeholder="Senha" type="password" name="password" id="password"  onBlur="if(this.value=='')this.value='sua senha...'" onFocus="if(this.value=='sua senha...')this.value='' "/>

									</div>
									<?php do_action( 'woocommerce_login_form' ); ?>

									<?php wp_nonce_field( 'woocommerce-login' ); ?>
									<!-- LINK RECUPERAE SENHA -->
									<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php _e( 'Esqueceu sua senha?', 'woocommerce' ); ?><i></i></a>
									<?php do_action( 'woocommerce_login_form_end' ); ?>

									<!-- BOTÃO ENTRAR -->
									<button type="submit" name="login" value="<?php _e( 'Entrar', 'woocommerce' ); ?>" >Entrar</button>

								</form>
								<a href="http://adegamalbec.pixd.com.br/wp-login.php?ywsl_social=facebook&redirect=http%3A%2F%2Fadegamalbec.pixd.com.br%2Fminha-conta%2F" class="face"><i class="fa fa-facebook" aria-hidden="true"></i>Entrar com facebook</a>
							</div>

						</div>
						<!-- ÁREA DE CADASTRO -->
						<div class="col-md-6">


							<div class="formulario cadastro">
								<p>não tenho cadastro</p>
								<span>Criar cadastro</span>
								<span class="obs">Receba promoções e ofertas exclusivas Salve endereço para as próximas compras</span>
								<form action="<?php echo get_permalink(woocommerce_get_page_id('myaccount')).'?action=register' ?>" method="GET">
									<input type="hidden" name="action" value="register">
									<input placeholder="Digite seu e-mail" name="email" type="text">
									<button type="submit" class="">Criar Cadastro</button>
								</form>
								<!-- CRIAR CONTA -->
								<?php //echo '<a  href="' .get_permalink(woocommerce_get_page_id('myaccount')). '?action=register?email"><button>Criar Cadastro</button></a>'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php endif; ?>




<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
