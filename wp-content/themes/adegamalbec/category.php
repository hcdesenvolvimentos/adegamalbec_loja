<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Dat services
 */

get_header(); ?>
	<!-- BLOG -->
	<div class="pg pg-blog" style="display:; ">
		
		<div class="container">	
				

			<div class="row">
				<div class="col-md-3 sidebar-blog">
				<!-- SUB TÍTULO DA PÁGINA -->
				<div class="sub-titulo">
					<p class="borda-titulo">Categorias</p>
				</div>
					<?php


					// CATEGORIA ATUAL
					$categoriaAtual = get_the_category();
					$categoriaAtual = $categoriaAtual[0]->cat_name;
					// LISTA DE CATEGORIAS
					$arrayCategorias = array();
					$categorias=get_categories($args);
					foreach($categorias as $categoria) {
					$arrayCategorias[$categoria->cat_ID] = $categoria->name;
					$nomeCategoria = $arrayCategorias[$categoria->cat_ID];


				?>
				<a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a>
				<?php } ?>
				</div>
				<div class="col-md-9">
				<?php 
								// LOOP DE DESTAQUE
					// LOOP DE DESTAQUE
								
				                
	               	if ( have_posts() ) : while( have_posts() ) : the_post();
					
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$foto = $foto[0];
				?>
					<div class="post">
						<!-- TÍTULO DO POST -->
						<div class="titulo-post">
							<a href="<?php echo get_permalink(); ?>"><p>"<?php echo get_the_title() ?> "</p></a>
							<span><!-- 6 comentários --></span>
						</div>
						<!-- DATA -->
						<span class="data">
							 <?php the_time('j \d\e F \d\e Y') ?>
							 - <?php the_time('g') ?> HORAS A TRÁS
						</span>
						<!-- IMAGEM -->
						<div class="bg-post" style="background: url(<?php echo $foto  ?>) no-repeat;"></div>
						<!-- DESCRIÇÃO -->
						<p class="descricao">
							<?php
								$content = get_the_content();
								$conteudo = substr($content, 0, 300).'...';
								echo $conteudo;
							?>
						</p>
						<!-- LINK CONTINUE LENDO -->
						<a href="<?php echo get_permalink(); ?>">continuar lendo</a>
					
					</div>
					
				<?php    endwhile; endif;wp_reset_query();   ?>
				</div>


				
			</div>

			<div class="paginador">
				
				<?php if (function_exists("pagination")) {
			    pagination();
				} 
				?>
			</div>
		</div>
	</div>
	

<?php get_footer(); ?>
