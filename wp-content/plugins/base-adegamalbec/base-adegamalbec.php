<?php

/**
 * Plugin Name: Base AdegaMalbec
 * Description: Controle base do tema AdegaMalbec.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseAdegaMalbec () {

		// TIPOS DE CONTEÚDO
		conteudosAdegaMalbec();

		// TAXONOMIA
		taxonomiaAdegaMalbec();

		// META BOXES
		metaboxesAdegaMalbec();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAdegaMalbec (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaAdegaMalbec () {

		taxonomiaCategoriaLocal();
	}
	
		// TAXONOMIA DE LOCAL
		function taxonomiaCategoriaLocal() {

			$rotulosCategoriaLocal = array(
												'name'              => 'Categorias de Local',
												'singular_name'     => 'Categoria de Local',
												'search_items'      => 'Buscar categorias de Local',
												'all_items'         => 'Todas categorias de Local',
												'parent_item'       => 'Categoria de Local pai',
												'parent_item_colon' => 'Categoria de Local pai:',
												'edit_item'         => 'Editar categoria de Local',
												'update_item'       => 'Atualizar categoria de Local',
												'add_new_item'      => 'Nova categoria de Local',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias de Local',
											);

			$argsCategoriaLocal 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaLocal,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-local' ),
											);

			register_taxonomy( 'categoriaLocal', array( 'local' ), $argsCategoriaLocal );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAdegaMalbec(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'AdegaMalbec_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Link do destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text'
					)
				),
			);

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAdegaMalbec(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAdegaMalbec(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAdegaMalbec');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAdegaMalbec();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );